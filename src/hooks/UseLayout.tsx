import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';

import { State } from 'reducer/RootReducer';
import {
    showLoad,
    hideLoad,
    changeDate,
    changeSearch,
    getList,
    getListDefault,
    getModalList,
    getModalListMonth,
    searchList,
    searchListReset,
    searchListEnd,
    changeCashEnd,
} from 'reducer/LoadReducer';

export const selectLoadState = (state: State) => state.LoadReducer.loadState;
export const selectDateState = (state: State) => state.LoadReducer.dateState;
export const selectListState = (state: State) => state.LoadReducer.listState;
export const selectListModalState = (state: State) => state.LoadReducer.listModalState;
export const selectRange = (state: State) => state.LoadReducer.range;
export const selectStartIdx = (state: State) => state.LoadReducer.startIdx;
export const selectEndIdx = (state: State) => state.LoadReducer.endIdx;
export const selectSearchStaus = (state: State) => state.LoadReducer.searchStaus;
export const selectSearchEnd = (state: State) => state.LoadReducer.searchEnd;
export const selectCashEnd = (state: State) => state.LoadReducer.cashEnd;
export const selectError = (state: State) => state.LoadReducer.error;

function UseLayout() {
    const dispatch = useDispatch();

    return {
        loadState: useSelector(selectLoadState),
        dateState: useSelector(selectDateState),
        listState: useSelector(selectListState),
        listModalState: useSelector(selectListModalState),
        range: useSelector(selectRange),
        startIdx: useSelector(selectStartIdx),
        endIdx: useSelector(selectEndIdx),
        searchStaus: useSelector(selectSearchStaus),
        searchEnd: useSelector(selectSearchEnd),
        cashEnd: useSelector(selectCashEnd),
        error: useSelector(selectError),
        showLoad: useCallback(bindActionCreators(showLoad, dispatch), [dispatch]),
        hideLoad: useCallback(bindActionCreators(hideLoad, dispatch), [dispatch]),
        changeDate: useCallback(bindActionCreators(changeDate, dispatch), [dispatch]),
        changeSearch: useCallback(bindActionCreators(changeSearch, dispatch), [dispatch]),
        getList: useCallback(bindActionCreators(getList, dispatch), [dispatch]),
        getListDefault: useCallback(bindActionCreators(getListDefault, dispatch), [dispatch]),
        getModalList: useCallback(bindActionCreators(getModalList, dispatch), [dispatch]),
        getModalListMonth: useCallback(bindActionCreators(getModalListMonth, dispatch), [dispatch]),
        searchList: useCallback(bindActionCreators(searchList, dispatch), [dispatch]),
        searchListReset: useCallback(bindActionCreators(searchListReset, dispatch), [dispatch]),
        searchListEnd: useCallback(bindActionCreators(searchListEnd, dispatch), [dispatch]),
        changeCashEnd: useCallback(bindActionCreators(changeCashEnd, dispatch), [dispatch]),
    };
}

export default UseLayout;
