import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';

import { State } from 'reducer/RootReducer';
import {
    postWrite,
    putWrite,
    getWrite,
    getWriteReset,
    setWriteLink,
    setCatagorySelect,
    initialState,
} from 'reducer/WriteReducer';

export const selectListState = (state: State) => state.WriteReducer.listState;
export const selectLangsLink = (state: State) => state.WriteReducer.langsLink;
export const selectCatagorySelect = (state: State) => state.WriteReducer.catagorySelect;
export const selectError = (state: State) => state.WriteReducer.error;

function UseWrite() {
    const dispatch = useDispatch();

    return {
        defaultState: initialState.listState[0],
        listState: useSelector(selectListState),
        langsLink: useSelector(selectLangsLink),
        catagorySelect: useSelector(selectCatagorySelect),
        error: useSelector(selectError),
        postWrite: useCallback(bindActionCreators(postWrite, dispatch), [dispatch]),
        putWrite: useCallback(bindActionCreators(putWrite, dispatch), [dispatch]),
        getWrite: useCallback(bindActionCreators(getWrite, dispatch), [dispatch]),
        getWriteReset: useCallback(bindActionCreators(getWriteReset, dispatch), [dispatch]),
        setWriteLink: useCallback(bindActionCreators(setWriteLink, dispatch), [dispatch]),
        setCatagorySelect: useCallback(bindActionCreators(setCatagorySelect, dispatch), [dispatch]),
    };
}

export default UseWrite;
