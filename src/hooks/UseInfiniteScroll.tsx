import { useState, useEffect } from 'react';
import useLayout from 'hooks/UseLayout';

const useInfiniteScroll = (callback?: any) => {
    const { searchEnd, searchStaus } = useLayout();
    const [isFetching, setIsFetching] = useState(false);

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, [searchStaus]);

    /*
    useEffect(() => {
        console.log('isFetching:', isFetching);
    });
    */

    useEffect(() => {
        //console.log('!!!!콜백')
        console.log('isFetching', isFetching);

        if (!isFetching) return;
        if (searchEnd) return;

        callback();
    }, [isFetching]);

    function handleScroll() {
        if (searchStaus) {
            if (
                window.innerHeight + document.documentElement.scrollTop + 5 <= document.documentElement.offsetHeight ||
                isFetching
            )
                return;
            setIsFetching(true);
        }
    }

    return { isFetching, setIsFetching };
};

export default useInfiniteScroll;
