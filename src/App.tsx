import React from 'react';
import 'assets/scss/Root.scss';

import Layout from 'container/Layout/Layout';

function App() {
    return (
        <React.Fragment>
            <Layout />
        </React.Fragment>
    );
}

export default App;
