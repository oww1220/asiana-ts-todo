import {combineReducers} from "redux";

import {LoadReducer, LoadState} from "reducer/LoadReducer";
import {WriteReducer, WriteState} from "reducer/WriteReducer";

const rootReducer = combineReducers({
    LoadReducer,
    WriteReducer,
});

export default rootReducer;

export type State = {
    LoadReducer: LoadState,
    WriteReducer: WriteState,
};
