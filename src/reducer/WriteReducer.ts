import * as Interface from 'interfaces/Interfaces';
import * as Commons from 'lib/Commons';
import { OmitResetState } from 'react-hook-form';

/*액션상수*/
export const POST_WRITE = 'WriteReducer/POST_WRITE' as const;
export const POST_WRITE_SUCCESS = 'WriteReducer/POST_WRITE_SUCCESS' as const;
export const POST_WRITE_FAILURE = 'WriteReducer/POST_WRITE_FAILURE' as const;
export const PUT_WRITE = 'WriteReducer/PUT_WRITE' as const;
export const PUT_WRITE_SUCCESS = 'WriteReducer/PUT_WRITE_SUCCESS' as const;
export const PUT_WRITE_FAILURE = 'WriteReducer/PUT_WRITE_FAILURE' as const;
export const GET_WRITE = 'WriteReducer/GET_WRITE' as const;
export const GET_WRITE_RESET = 'WriteReducer/GET_WRITE_RESET' as const;
export const GET_WRITE_SUCCESS = 'WriteReducer/GET_WRITE_SUCCESS' as const;
export const GET_WRITE_FAILURE = 'WriteReducer/GET_WRITE_FAILURE' as const;
export const SET_WRITE_LINK = 'WriteReducer/SET_WRITE_LINK' as const;
export const SET_CATAGORY_SELECT = 'WriteReducer/SET_CATAGORY_SELECT' as const;

/*액션함수*/
export const postWrite = (data: Interface.ListState, history: any) => ({
    type: POST_WRITE,
    payload: { data, history }
});
export const postWriteSuccess = () => ({ type: POST_WRITE_SUCCESS });
export const postWriteFailure = (error: boolean) => ({ type: POST_WRITE_FAILURE, payload: { error } });
export const putWrite = (data: Interface.ListState, id: string, history: any) => ({
    type: PUT_WRITE,
    payload: { data, id, history }
});
export const putWriteSuccess = () => ({ type: PUT_WRITE_SUCCESS });
export const putWriteFailure = (error: boolean) => ({ type: PUT_WRITE_FAILURE, payload: { error } });
export const getWrite = (
    id: string,
    reset: (values?: Record<string, any>, omitResetState?: OmitResetState) => void
) => ({
    type: GET_WRITE,
    payload: { id, reset }
});
export const getWriteReset = () => ({ type: GET_WRITE_RESET });
export const getWriteSuccess = (data: Interface.ListState[]) => ({ type: GET_WRITE_SUCCESS, payload: { data } });
export const getWriteFailure = (error: boolean) => ({ type: GET_WRITE_FAILURE, payload: { error } });
export const setWriteLink = (langsLink: Interface.LangsLink) => ({ type: SET_WRITE_LINK, payload: { langsLink } });
export const setCatagorySelect = (catagorySelect: string) => ({
    type: SET_CATAGORY_SELECT,
    payload: { catagorySelect }
});

/*기본상태 및 리듀서*/
export interface WriteState {
    listState: Interface.ListState[];
    langsLink: Interface.LangsLink;
    catagorySelect: string;
    error: boolean;
}

export const initialState: WriteState = {
    listState: [
        {
            date: Commons.dateCheck()[0],
            chdate: Commons.dateCheck()[0],
            day: Commons.dateCheck()[1],
            cid: '',
            sort: '',
            name: '',
            catagory: '',
            lang: '',
            koUrl: '',
            viUrl: '',
            esUrl: '',
            enUrl: '',
            jaUrl: '',
            chUrl: '',
            zhUrl: '',
            ruUrl: '',
            deUrl: '',
            frUrl: '',
            state: 'normal',
            cording: '',
            request: '',
            etc: '',
            id: 0,
            chk: [],
            chkall: false
        }
    ],
    langsLink: {
        KO: false,
        VI: false,
        ES: false,
        EN: false,
        JA: false,
        CH: false,
        ZH: false,
        RU: false,
        DE: false,
        FR: false
    },
    catagorySelect: '',
    error: false
};

type WriteAction =
    | ReturnType<typeof postWrite>
    | ReturnType<typeof postWriteSuccess>
    | ReturnType<typeof postWriteFailure>
    | ReturnType<typeof putWrite>
    | ReturnType<typeof putWriteSuccess>
    | ReturnType<typeof putWriteFailure>
    | ReturnType<typeof getWrite>
    | ReturnType<typeof getWriteReset>
    | ReturnType<typeof getWriteSuccess>
    | ReturnType<typeof getWriteFailure>
    | ReturnType<typeof setWriteLink>
    | ReturnType<typeof setCatagorySelect>;

export function WriteReducer(state: WriteState = initialState, action: WriteAction): WriteState {
    switch (action.type) {
        case POST_WRITE:
            return {
                ...state
            };
        case POST_WRITE_SUCCESS:
            return {
                ...state
            };
        case POST_WRITE_FAILURE:
            return {
                ...state,
                error: true
            };

        case PUT_WRITE:
            return {
                ...state
            };
        case PUT_WRITE_SUCCESS:
            return {
                ...state
            };
        case PUT_WRITE_FAILURE:
            return {
                ...state,
                error: true
            };

        case GET_WRITE:
            return {
                ...state
            };
        case GET_WRITE_RESET:
            return {
                ...state,
                listState: initialState.listState,
                langsLink: initialState.langsLink,
                catagorySelect: ''
            };
        case GET_WRITE_SUCCESS:
            return {
                ...state,
                listState: action.payload.data
            };
        case GET_WRITE_FAILURE:
            return {
                ...state,
                error: true
            };
        case SET_WRITE_LINK:
            return {
                ...state,
                langsLink: action.payload.langsLink
            };
        case SET_CATAGORY_SELECT:
            return {
                ...state,
                catagorySelect: action.payload.catagorySelect
            };

        default:
            return state;
    }
}
