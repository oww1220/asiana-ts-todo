import * as Interface from 'interfaces/Interfaces';

/*액션상수*/
export const SHOW_LOAD = 'LoadReducer/SHOW_LOAD' as const;
export const HIDE_LOAD = 'LoadReducer/HIDE_LOAD' as const;
export const CHANGE_DATE = 'LoadReducer/CHANGE_DATE' as const;
export const CHANGE_SEARCH = 'LoadReducer/CHANGE_SEARCH' as const;
export const CHANGE_CASHEND = 'LoadReducer/CHANGE_CASHEND' as const;
export const GET_LIST = 'LoadReducer/GET_LIST' as const;
export const GET_LIST_DEFAULT = 'LoadReducer/GET_LIST_DEFAULT' as const;
export const GET_LIST_SUCCESS = 'LoadReducer/GET_LIST_SUCCESS' as const;
export const GET_LIST_FAILURE = 'LoadReducer/GET_LIST_FAILURE' as const;

export const GET_LIST_MODAL = 'LoadReducer/GET_LIST_MODAL' as const;
export const GET_LIST_MODAL_SUCCESS = 'LoadReducer/GET_LIST_MODAL_SUCCESS' as const;
export const GET_LIST_MODAL_FAILURE = 'LoadReducer/GET_LIST_MODAL_FAILURE' as const;

export const GET_LIST_MODAL_MONTH = 'LoadReducer/GET_LIST_MODAL_MONTH' as const;
export const GET_LIST_MODAL_MONTH_SUCCESS = 'LoadReducer/GET_LIST_MODAL_MONTH_SUCCESS' as const;
export const GET_LIST_MODAL_MONTH_FAILURE = 'LoadReducer/GET_LIST_MODAL_MONTH_FAILURE' as const;

export const SEARCH_LIST = 'LoadReducer/SEARCH_LIST' as const;
export const SEARCH_LIST_RESET = 'LoadReducer/SEARCH_LIST_RESET' as const;
export const SEARCH_LIST_END = 'LoadReducer/SEARCH_LIST_END' as const;
export const SEARCH_LIST_SUCCESS = 'LoadReducer/SEARCH_LIST_SUCCESS' as const;
export const SEARCH_LIST_FAILURE = 'LoadReducer/SEARCH_LIST_FAILURE' as const;

/*액션함수*/
export const showLoad = () => ({ type: SHOW_LOAD });
export const hideLoad = () => ({ type: HIDE_LOAD });
export const changeDate = (date: Date) => ({ type: CHANGE_DATE, payload: { date } });
export const changeSearch = (searchStaus: boolean) => ({ type: CHANGE_SEARCH, payload: { searchStaus } });
export const changeCashEnd = (cashEnd: number) => ({ type: CHANGE_CASHEND, payload: { cashEnd } });

/*리스트 불러오기*/
export const getList = (date: Date) => ({ type: GET_LIST, payload: { date } });
export const getListDefault = () => ({ type: GET_LIST_DEFAULT });
export const getListSuccess = (data: Interface.ListState[]) => ({ type: GET_LIST_SUCCESS, payload: { data } });
export const getListFailure = (error: boolean) => ({ type: GET_LIST_FAILURE, error });

/*주간보기*/
export const getModalList = (range: string[]) => ({ type: GET_LIST_MODAL, payload: { range } });
export const getModalListSuccess = (data: Interface.ListState[], range: string[]) => ({
    type: GET_LIST_MODAL_SUCCESS,
    payload: { data, range },
});
export const getModalListFailure = (error: boolean) => ({ type: GET_LIST_MODAL_FAILURE, error });

/*월간보기*/
export const getModalListMonth = (date: Date) => ({ type: GET_LIST_MODAL_MONTH, payload: { date } });
export const getModalListMonthSuccess = (data: Interface.ListState[]) => ({
    type: GET_LIST_MODAL_MONTH_SUCCESS,
    payload: { data },
});
export const getModalListMonthFailure = (error: boolean) => ({ type: GET_LIST_MODAL_MONTH_FAILURE, error });

export const searchList = (
    txt: string,
    startIdx: number,
    endIdx: number,
    startDate: Date | null,
    endDate: Date | null,
    callback: (val: string) => any,
) => ({
    type: SEARCH_LIST,
    payload: { txt, startIdx, endIdx, startDate, endDate, callback },
});
export const searchListReset = () => ({ type: SEARCH_LIST_RESET });
export const searchListEnd = (searchEnd: boolean) => ({ type: SEARCH_LIST_END, payload: { searchEnd } });
export const searchListSuccess = (data: Interface.ListState[], startIdx: number, endIdx: number) => ({
    type: SEARCH_LIST_SUCCESS,
    payload: { data, startIdx, endIdx },
});
export const searchListFailure = (error: boolean) => ({ type: SEARCH_LIST_FAILURE, error });

/*기본상태 및 리듀서*/
export interface LoadState {
    loadState: boolean;
    dateState: Date;
    listState: Interface.ListState[];
    listModalState: Interface.ListState[];
    range: string[];
    startIdx: number;
    endIdx: number;
    searchStaus: boolean;
    searchEnd: boolean;
    cashEnd: number;
    error: boolean;
}

const initialState: LoadState = {
    loadState: false,
    dateState: new Date(),
    listState: [],
    listModalState: [],
    range: [],
    startIdx: 0,
    endIdx: 0,
    searchStaus: false,
    searchEnd: false,
    cashEnd: 0,
    error: false,
};

type LoadAction =
    | ReturnType<typeof showLoad>
    | ReturnType<typeof hideLoad>
    | ReturnType<typeof changeDate>
    | ReturnType<typeof changeSearch>
    | ReturnType<typeof changeCashEnd>
    | ReturnType<typeof getList>
    | ReturnType<typeof getListDefault>
    | ReturnType<typeof getListSuccess>
    | ReturnType<typeof getListFailure>
    | ReturnType<typeof getModalList>
    | ReturnType<typeof getModalListSuccess>
    | ReturnType<typeof getModalListFailure>
    | ReturnType<typeof getModalListMonth>
    | ReturnType<typeof getModalListMonthSuccess>
    | ReturnType<typeof getModalListMonthFailure>
    | ReturnType<typeof searchList>
    | ReturnType<typeof searchListReset>
    | ReturnType<typeof searchListEnd>
    | ReturnType<typeof searchListSuccess>
    | ReturnType<typeof searchListFailure>;

export function LoadReducer(state: LoadState = initialState, action: LoadAction): LoadState {
    switch (action.type) {
        case SHOW_LOAD:
            return {
                ...state,
                loadState: true,
            };

        case HIDE_LOAD:
            return {
                ...state,
                loadState: false,
            };

        case CHANGE_DATE:
            return {
                ...state,
                dateState: action.payload.date,
            };

        case CHANGE_SEARCH:
            return {
                ...state,
                searchStaus: action.payload.searchStaus,
            };

        case CHANGE_CASHEND:
            return {
                ...state,
                cashEnd: action.payload.cashEnd,
            };

        case GET_LIST:
            return {
                ...state,
                searchEnd: false,
                loadState: false,
            };

        case GET_LIST_DEFAULT:
            return {
                ...state,
                searchEnd: false,
                loadState: false,
            };
        case GET_LIST_SUCCESS:
            return {
                ...state,
                listState: action.payload.data,
                searchStaus: false,
                loadState: false,
            };
        case GET_LIST_FAILURE:
            return {
                ...state,
                error: true,
            };
        case GET_LIST_MODAL:
            return {
                ...state,
                searchEnd: false,
                loadState: false,
            };

        case GET_LIST_MODAL_SUCCESS:
            return {
                ...state,
                listModalState: action.payload.data,
                range: action.payload.range,
                searchStaus: false,
                loadState: false,
            };
        case GET_LIST_MODAL_FAILURE:
            return {
                ...state,
                error: true,
            };

        case GET_LIST_MODAL_MONTH:
            return {
                ...state,
                searchEnd: false,
                loadState: false,
            };
        case GET_LIST_MODAL_MONTH_SUCCESS:
            return {
                ...state,
                listModalState: action.payload.data,
                searchStaus: false,
                loadState: false,
            };
        case GET_LIST_MODAL_MONTH_FAILURE:
            return {
                ...state,
                error: true,
            };

        case SEARCH_LIST:
            return {
                ...state,
                loadState: false,
            };

        case SEARCH_LIST_RESET:
            return {
                ...state,
                loadState: false,
                startIdx: 0,
                endIdx: 0,
                cashEnd: 0,
                listState: [],
            };

        case SEARCH_LIST_END:
            return {
                ...state,
                loadState: false,
                searchEnd: action.payload.searchEnd,
            };

        case SEARCH_LIST_SUCCESS:
            return {
                ...state,
                listState: [...state.listState, ...action.payload.data],
                startIdx: action.payload.startIdx,
                endIdx: action.payload.endIdx,
                searchStaus: true,
            };
        case SEARCH_LIST_FAILURE:
            return {
                ...state,
                error: true,
            };

        default:
            return state;
    }
}
