import React from 'react';
import { call, put, select } from 'redux-saga/effects';
import * as matchers from 'redux-saga-test-plan/matchers';
import { expectSaga } from 'redux-saga-test-plan';
import { throwError } from 'redux-saga-test-plan/providers';

import * as Api from 'lib/Api';
import * as Const from '__test__/Mock/Const';
import * as ActionsLoad from 'reducer/LoadReducer';
import { watchSearchList } from 'saga/SearchSaga';
import { selectCashEnd } from 'hooks/UseLayout';

describe('watchSearchList', () => {
    it('SearchList success 데이터가 20개 이하일시!!', () => {
        return (
            expectSaga(watchSearchList)
                //리듀서
                .withReducer(ActionsLoad.LoadReducer)
                //사가가 observing하는 액션 디스페치
                .dispatch(
                    ActionsLoad.searchList(
                        Const.Fake_searchListParam.txt,
                        Const.Fake_searchListParam.startIdx,
                        Const.Fake_searchListParam.endIdx,
                        Const.Fake_searchListParam.startDate,
                        Const.Fake_searchListParam.endDate,
                        Const.Fake_searchListParam.callback,
                    ),
                )
                //mock date생성
                .provide([
                    [select(selectCashEnd), 0], //캐쉬된 값으로 채크 cashEnd === endIdx 면 중복으로 판별!!
                    [
                        call(
                            Api.searchListItem,
                            Const.Fake_searchListParam.txt, //call mock인경우는 실제 함수랑 돌아가는 환경을 맞추야지 mock데이터가 들어감..안그럼 기존 호출 데이터가 나옴 !!
                            Const.Fake_searchListParam.startIdx + Const.Fake_increase, //한번에 불러오는 상수값 증감
                            Const.Fake_searchListParam.endIdx + Const.Fake_increase,
                            Const.Fake_searchListParam.startDate,
                            Const.Fake_searchListParam.endDate,
                        ),
                        Const.Fake_listState,
                    ], //mock 데이터 삽입
                ])

                //사가 내부 action 들-----
                .put(ActionsLoad.showLoad())
                .put(ActionsLoad.changeCashEnd(Const.Fake_searchListParam.endIdx + Const.Fake_increase)) //중복호출을 막기위해 endIdx를 저장한다! 다음호출시 파라미터로 들어오는endIdx와비교
                .put(ActionsLoad.searchListEnd(true)) //불러온 배열이 20개 이하일시 endFlag활성화!!
                .put(
                    ActionsLoad.searchListSuccess(
                        Const.Fake_listState.data,
                        Const.Fake_searchListParam.startIdx + Const.Fake_increase,
                        Const.Fake_searchListParam.endIdx + Const.Fake_increase,
                    ),
                )
                .put(ActionsLoad.hideLoad())
                .silentRun()
        );
    });

    it('SearchList failure!!', () => {
        const error = throwError(new Error('SearchList error!!'));

        return (
            expectSaga(watchSearchList)
                //리듀서
                .withReducer(ActionsLoad.LoadReducer)
                //사가가 observing하는 액션 디스페치
                .dispatch(
                    ActionsLoad.searchList(
                        Const.Fake_searchListParam.txt,
                        Const.Fake_searchListParam.startIdx,
                        Const.Fake_searchListParam.endIdx,
                        Const.Fake_searchListParam.startDate,
                        Const.Fake_searchListParam.endDate,
                        Const.Fake_searchListParam.callback,
                    ),
                )
                //mock date생성
                .provide([
                    [select(selectCashEnd), 0], //캐쉬된 값으로 채크 cashEnd === endIdx 면 중복으로 판별!!
                    [
                        call(
                            Api.searchListItem,
                            Const.Fake_searchListParam.txt, //call mock인경우는 실제 함수랑 돌아가는 환경을 맞추야지 mock데이터가 들어감..안그럼 기존 호출 데이터가 나옴 !!
                            Const.Fake_searchListParam.startIdx + Const.Fake_increase, //한번에 불러오는 상수값 증감
                            Const.Fake_searchListParam.endIdx + Const.Fake_increase,
                            Const.Fake_searchListParam.startDate,
                            Const.Fake_searchListParam.endDate,
                        ),
                        error,
                    ], //에러 mock 데이터 삽입
                ])
                .put(ActionsLoad.showLoad())
                .put(ActionsLoad.searchListFailure(error.response))
                .put(ActionsLoad.hideLoad())
                .silentRun()
        );
    });
});
