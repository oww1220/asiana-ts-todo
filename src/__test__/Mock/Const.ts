export const Fake_searchListParam = {
    txt: '지우',
    startIdx: -10,
    endIdx: 10,
    startDate: null,
    endDate: null,
    callback: (txt: string) => {},
};

export const Fake_increase = 10;

//axios를 통해 받아진 데이터는 obg로 한번 맵핑 됨
export const Fake_listState = {
    data:
        //<---api로 받는 실제 데이터
        [
            {
                catagory: '약관',
                chUrl: 'http://localhost/asiana_manage/contents/terms/dsaf_trems_ch_pc.php',
                chdate: '2021-02-25',
                chk: ['KO', 'VI', 'ES', 'EN', 'JA', 'CH', 'ZH', 'RU', 'DE', 'FR'],
                cid: 'dsaf',
                cording: '정진호',
                date: '2021-02-25',
                day: '목',
                deUrl: 'http://localhost/asiana_manage/contents/terms/dsaf_trems_de_pc.php',
                enUrl: 'http://localhost/asiana_manage/contents/terms/dsaf_trems_en_pc.php',
                esUrl: 'http://localhost/asiana_manage/contents/terms/dsaf_trems_es_pc.php',
                etc: '',
                frUrl: 'http://localhost/asiana_manage/contents/terms/dsaf_trems_fr_pc.php',
                id: 1302,
                jaUrl: 'http://localhost/asiana_manage/contents/terms/dsaf_trems_ja_pc.php',
                koUrl: 'http://localhost/asiana_manage/contents/terms/dsaf_trems_ko_pc.php',
                lang: 'PC',
                name: 'sadf',
                request: '김미선',
                ruUrl: 'http://localhost/asiana_manage/contents/terms/dsaf_trems_ru_pc.php',
                sort: '신규',
                state: 'normal',
                viUrl: 'http://localhost/asiana_manage/contents/terms/dsaf_trems_vi_pc.php',
                zhUrl: 'http://localhost/asiana_manage/contents/terms/dsaf_trems_zh_pc.php',
            },
        ],
    //api로 받는 실제 데이터--->
};
