import { takeEvery, put, call, select } from 'redux-saga/effects';

import * as Api from 'lib/Api';
import * as ActionsWrite from 'reducer/WriteReducer';
import * as ActionsLoad from 'reducer/LoadReducer';

function* getWriteSaga(action: ReturnType<typeof ActionsWrite.getWrite>) {
    console.log('saga!:', action);

    yield put(ActionsLoad.showLoad());

    try {
        const id = action.payload.id;
        const { data } = yield call(Api.getListItem, id);
        //console.log(data);
        yield put(ActionsWrite.getWriteSuccess(data));
        yield put(
            ActionsWrite.setWriteLink({
                KO: !!data[0].koUrl,
                VI: !!data[0].viUrl,
                ES: !!data[0].esUrl,
                EN: !!data[0].enUrl,
                JA: !!data[0].jaUrl,
                CH: !!data[0].chUrl,
                ZH: !!data[0].zhUrl,
                RU: !!data[0].ruUrl,
                DE: !!data[0].deUrl,
                FR: !!data[0].frUrl
            })
        );
        yield put(ActionsWrite.setCatagorySelect(data[0].catagory));

        const values = data[0].cid ? data[0] : { ...data[0], cid: '' };
        //console.log(values);
        action.payload.reset(values);
    } catch (error) {
        yield put(ActionsWrite.getWriteFailure(error.response));
    }

    yield put(ActionsLoad.hideLoad());
}

function* postWriteSaga(action: ReturnType<typeof ActionsWrite.postWrite>) {
    console.log('saga!:', action);

    yield put(ActionsLoad.showLoad());

    try {
        const { request } = yield call(Api.insertList, action.payload.data);
        //console.log(request);
        if (request.status === 201) {
            yield put(ActionsWrite.postWriteSuccess());
            yield put(ActionsLoad.hideLoad());
            action.payload.history.push('/');
        }
    } catch (error) {
        yield put(ActionsWrite.postWriteFailure(error.response));
    }
}

function* putWriteSaga(action: ReturnType<typeof ActionsWrite.putWrite>) {
    yield put(ActionsLoad.showLoad());

    try {
        const { request } = yield call(Api.updateList, action.payload.data, action.payload.id);
        //console.log(request);
        if (request.status === 200) {
            yield put(ActionsWrite.putWriteSuccess());
            yield put(ActionsLoad.hideLoad());
            action.payload.history.push('/');
        }
    } catch (error) {
        yield put(ActionsWrite.putWriteFailure(error.response));
    }
}

export function* watchGetWrite() {
    yield takeEvery(ActionsWrite.GET_WRITE, getWriteSaga);
}

export function* watchPostWrite() {
    yield takeEvery(ActionsWrite.POST_WRITE, postWriteSaga);
}

export function* watchPutWrite() {
    yield takeEvery(ActionsWrite.PUT_WRITE, putWriteSaga);
}
