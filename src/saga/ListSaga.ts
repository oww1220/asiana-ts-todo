import { takeEvery, put, call } from 'redux-saga/effects';

import * as Api from 'lib/Api';
import * as ActionsLoad from 'reducer/LoadReducer';

function* getListSaga(action: ReturnType<typeof ActionsLoad.getList>) {
    console.log('saga!:', action);

    yield put(ActionsLoad.showLoad());

    try {
        const dateState = action.payload.date;
        const { data } = yield call(Api.getList, dateState);
        console.log(data);
        yield put(ActionsLoad.getListSuccess(data));
    } catch (error) {
        yield put(ActionsLoad.getListFailure(error.response));
    }
}

function* getListDefaultSaga(action: ReturnType<typeof ActionsLoad.getListDefault>) {
    console.log('saga!:', action);

    yield put(ActionsLoad.showLoad());

    try {
        const { data } = yield call(Api.getListDefault);
        console.log(data);
        yield put(ActionsLoad.getListSuccess(data));
    } catch (error) {
        yield put(ActionsLoad.getListFailure(error.response));
    }
}

function* getModalListSaga(action: ReturnType<typeof ActionsLoad.getModalList>) {
    console.log('saga!:', action);

    yield put(ActionsLoad.showLoad());

    try {
        const range = action.payload.range;
        const { data } = yield call(Api.getListModalItem, range);
        console.log(data);
        yield put(ActionsLoad.getModalListSuccess(data, range));
    } catch (error) {
        yield put(ActionsLoad.getModalListFailure(error.response));
    }
}

function* getModalListMonthSaga(action: ReturnType<typeof ActionsLoad.getModalListMonth>) {
    console.log('saga!:', action);

    yield put(ActionsLoad.showLoad());

    try {
        const date = action.payload.date;
        const { data } = yield call(Api.getListModalItemMonth, date);
        console.log(data);
        yield put(ActionsLoad.getModalListMonthSuccess(data));
    } catch (error) {
        yield put(ActionsLoad.getModalListMonthFailure(error.response));
    }
}

export function* watchGetList() {
    yield takeEvery(ActionsLoad.GET_LIST, getListSaga);
}

export function* watchGetListDefault() {
    yield takeEvery(ActionsLoad.GET_LIST_DEFAULT, getListDefaultSaga);
}

export function* watchGetModalList() {
    yield takeEvery(ActionsLoad.GET_LIST_MODAL, getModalListSaga);
}

export function* watchGetModalListMonth() {
    yield takeEvery(ActionsLoad.GET_LIST_MODAL_MONTH, getModalListMonthSaga);
}
