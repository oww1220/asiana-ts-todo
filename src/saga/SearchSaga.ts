import { takeEvery, put, call, select, delay } from 'redux-saga/effects';

import * as Api from 'lib/Api';
import * as ActionsLoad from 'reducer/LoadReducer';
import { selectCashEnd } from 'hooks/UseLayout';

function* searchListSaga(action: ReturnType<typeof ActionsLoad.searchList>): Generator<any, void, any> {
    console.log('saga!:', action);

    const increase: number = 10;

    yield put(ActionsLoad.showLoad());

    try {
        const cashEnd = yield select(selectCashEnd);

        const { txt, startDate, endDate } = action.payload;
        const startIdx = action.payload.startIdx + increase === 10 ? 20 : action.payload.startIdx + increase;
        const endIdx = action.payload.endIdx + increase;
        console.log(startIdx, endIdx, cashEnd);
        if (cashEnd === endIdx) {
            console.log('!!!중복');
            yield put(ActionsLoad.hideLoad());
            return;
        }
        yield put(ActionsLoad.changeCashEnd(endIdx));

        const callback = action.payload.callback;
        const { data } = yield call(Api.searchListItem, txt, startIdx, endIdx, startDate, endDate);
        console.log('axios!!!!!!', data);
        if (!data.length || (startIdx === 0 && data.length < 20)) {
            console.log('data.lengh:', data.length);
            yield put(ActionsLoad.searchListEnd(true));
        }
        yield put(ActionsLoad.searchListSuccess(data, startIdx, endIdx));
        //if (txt !== '') yield call(callback(txt));
        if (txt !== '') yield call(callback, txt);
    } catch (error) {
        console.log('error!!!', error);
        yield put(ActionsLoad.searchListFailure(error.response));
    }

    yield put(ActionsLoad.hideLoad());
}

export function* watchSearchList() {
    yield takeEvery(ActionsLoad.SEARCH_LIST, searchListSaga);
}
