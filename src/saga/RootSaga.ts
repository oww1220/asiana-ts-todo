import { all, fork } from 'redux-saga/effects';

import { watchGetList, watchGetListDefault, watchGetModalList, watchGetModalListMonth } from 'saga/ListSaga';
import { watchSearchList } from 'saga/SearchSaga';
import { watchGetWrite, watchPostWrite, watchPutWrite } from 'saga/WriteSaga';

export default function* rootSaga() {
    yield all([
        fork(watchGetList),
        fork(watchGetListDefault),
        fork(watchGetModalList),
        fork(watchGetModalListMonth),
        fork(watchSearchList),
        fork(watchGetWrite),
        fork(watchPostWrite),
        fork(watchPutWrite)
    ]);
}
