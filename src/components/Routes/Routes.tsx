import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Home from 'container/Home/Home';
import IeCopyWrap from 'container/IeCopyWrap/IeCopyWrap';
import MapToAnchor from 'container/MapToAnchor/MapToAnchor';
import VwCalculator from 'container/VwCalculator/VwCalculator';
import CkEditor from 'container/CkEditor/CkEditor';
import SortSource from 'container/SortSource/SortSource';
import UnEscape from 'container/UnEscape/UnEscape';
import NotFound from 'container/NotFound/NotFound';
import ScrollToTop from 'components/ScrollToTop/ScrollToTop';
import Write from 'container/Write/Write';
import TestPage from 'container/TestPage/TestPage';

function Routes() {
    return (
        <>
            <ScrollToTop />
            <Switch>
                <Route path="/" component={Home} exact={true} />
                <Route path="/Write/:name" component={Write} />
                <Route path="/Write" component={Write} />
                <Route path="/IeCopyWrap" component={IeCopyWrap} />
                <Route path="/MapToAnchor" component={MapToAnchor} />
                <Route path="/VwCalculator" component={VwCalculator} />
                <Route path="/SortSource" component={SortSource} />
                <Route path="/CkEditor" component={CkEditor} />
                <Route path="/UnEscape" component={UnEscape} />
                <Route path="/TestPage" component={TestPage} />

                <Route component={NotFound} />
            </Switch>
        </>
    );
}

export default Routes;
