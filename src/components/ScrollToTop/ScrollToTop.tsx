import { useEffect } from 'react';
import useReactRouter from 'use-react-router';

function ScrollToTop() {
    const { history, location, match } = useReactRouter();

    useEffect(() => {
        const unlisten = history.listen(() => {
            window.scrollTo(0, 0);
        });
        return () => {
            unlisten();
        };
    }, []);

    return null;
}

export default ScrollToTop;
