import React, { useEffect, useState } from 'react';
import { Ref, MultipleFieldErrors, Message } from 'react-hook-form';
import cx from 'classnames';
import * as Interface from 'interfaces/Interfaces';
import jquery from 'jquery';

const $: JQueryStatic = jquery;

interface FieldError {
    type: string;
    ref?: Ref;
    types?: MultipleFieldErrors;
    message?: Message;
}

interface InputProps {
    type: string;
    name: string;
    errors: any;
    defaultValue?: string | number;
    readOnly?: boolean;
    style?: Interface.IClassNames;
    placeholder?: string;
    list?: string[];
    handleChangeP?(e: React.ChangeEvent<any>): void;
    handleClickP?(e: React.ChangeEvent<any>): void;
}

function Input(
    { type, name, errors, defaultValue, readOnly, style, placeholder, list, handleChangeP, handleClickP }: InputProps,
    ref: React.Ref<any>
) {
    const [inputFlag, setInputFlag] = useState(false);

    const handleClick = () => {};
    const handleChange = () => {};
    const handleBlur = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        if (e.target.value) {
            setInputFlag(true);
        } else {
            setInputFlag(false);
        }
    };

    const FieldError: FieldError = errors[name];

    useEffect(() => {
        setInputFlag(false);
        if (defaultValue) setInputFlag(true);
    }, [defaultValue]);

    if (type === 'radio') {
        const inputR =
            list &&
            list.map((item, idx) => {
                //console.log(item, idx);
                return (
                    <label key={idx} htmlFor={name + idx} style={{ marginLeft: idx == 0 ? 0 : '20px' }}>
                        {
                            <input
                                type={type}
                                name={name}
                                id={name + idx}
                                value={item}
                                ref={ref}
                                defaultChecked={defaultValue === item}
                                //checked={defaultValue === item}
                                onChange={handleChangeP ? handleChangeP : handleChange}
                                onClick={handleClickP ? handleClickP : handleClick}
                            />
                        }
                        <span>{item}</span>
                    </label>
                );
            });

        return (
            <>
                {inputR}
                {FieldError && <p className="err">{FieldError.message}</p>}
            </>
        );
    } else if (type === 'textarea') {
        return (
            <>
                <textarea
                    name={name}
                    ref={ref}
                    style={style}
                    placeholder={placeholder}
                    defaultValue={defaultValue}
                    className={cx([], { on: inputFlag })}
                    onBlur={handleBlur}
                    onChange={handleChangeP ? handleChangeP : handleChange}
                >
                    {}
                </textarea>
                {FieldError && <p className="err">{FieldError.message}</p>}
            </>
        );
    } else {
        return (
            <>
                <label style={{ display: 'none' }}>{name}</label>
                <input
                    type={type}
                    name={name}
                    ref={ref}
                    defaultValue={defaultValue}
                    readOnly={readOnly}
                    style={style}
                    placeholder={placeholder}
                />
                {FieldError && <p className="err">{FieldError.message}</p>}
            </>
        );
    }
}

export default React.forwardRef(Input);
