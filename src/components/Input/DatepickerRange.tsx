import React, { useState, useEffect, useRef, useCallback, forwardRef, useImperativeHandle } from 'react';
import DatePicker, { registerLocale } from 'react-datepicker';
import Moment from 'react-moment';
import ko from 'date-fns/locale/ko';
import * as Commons from 'lib/Commons';
import 'react-datepicker/dist/react-datepicker.css';

registerLocale('ko', ko);

type ButtonRef = HTMLButtonElement;

const years = Commons.range(2018, Commons.getYear(new Date()) + 1);
const months = ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'];

const DatepickerRangeBt = forwardRef<ButtonRef, React.ComponentPropsWithoutRef<'button'>>((props, ref) => {
    //console.log('props', props);
    //왜그런지 모르겠으나 react-datepicker package 커스텀버튼에 넘기는 ref는 감지가 안됨;;;;;;
    return (
        <button className="custom-input" onClick={props.onClick} ref={ref} id={props.id} value={props.value}>
            {props.value ? <Moment date={props.value} format="YYYY년MM월DD일" /> : '0000년00월00일'}
        </button>
    );
});

//test용 컴퍼넌트 입니다!!!
//child 컴퍼넌트에 ref를 주입시키기위해서는 forwardRef스코프를 고차함수방식(함수를 인자로 받아서 함수를 리턴)으로 랩핑한다..
const DatepickerTestBt = forwardRef<ButtonRef, React.ComponentPropsWithoutRef<'button'>>((props, ref) => {
    //console.log('props', props);
    const childRef = useRef<ButtonRef>(null);
    const [testNumber, setTestNumber] = useState<number>(0);

    // 1번인자 : ref
    // 2번인자 : 현재 컴포의 값을 부모컴포가 접근할수 있는 CB(콜백)함수를 전달
    // ref에 콜백함수가 리턴하는 값으로 바꿔치기해서 부모측의 ref current참조 대상이 바뀜
    // 즉 useRef는 참조값을 담는 그릇을 만드는 기능일뿐
    // useImperativeHandle사용시에는 자식 컴퍼넌트는 새로운 자기자신의 ref를 만들어서 사용: childRef 예시
    useImperativeHandle<any, any>(ref, () => ({
        //버튼 ref에 맞는 형식으로 넘기야됨, ref가 any타입이면 아무런 이름으로 할수있음 !
        click: () => {
            setTestNumber(testNumber + 1);
            if (childRef.current) console.log(childRef.current.value);
        },
    })); //3번째 파리미터로 의존성 배열을 삽입가능해 호출 횟수 조절가능
    //코드를 자세히보면 우리가 넣은 2개의 argument 에서 처음은 부모가 보낸 reference,
    //그리고 두번째는 자식이 맞춰 보낼 reference로 이는 부모가 reference.current로 접속할 수 있게 맵핑을 해주고 있다.
    //즉 부모에게 꼭 자식의 실제 reference를 보내지 않고 우리가 원하는 일종의 proxy reference를 보내는게 가능해진다는 것이다.
    //이는 함수형 컴포넌트에서 가질 수 없는 내부 method를 외부에 보낼수 있다는 것을 의미하며 또한 내부 자식을 private으로 그대로 남겨둘 수 있다는 것이기도 하다.
    //위에서보면 부모는 자식의 DOM에 직접적으로 접근을 하는 것이 아니라 useImperativeHandle로 전달된 메서드에만 접근이 가능해진다. 이로서 더욱 컴포넌트간의 독립성을 보장할 수가 있게 되는 것이다.

    useEffect(() => {
        console.log('testNumber', testNumber);
    }, [testNumber]);

    return (
        <button className="custom-input" ref={childRef} value="test!!">
            {`forwardRedf test ${testNumber}`}
        </button>
    );
});

const DatepickerRange = () => {
    const datePickerRef = useRef<ButtonRef>(null);
    const [startDate, setStartDate] = useState<null | Date>(null);
    const [endDate, setEndDate] = useState<null | Date>(null);

    //useEffect(() => {
    //console.log('datePickerRef.current!!!!!!!!', datePickerRef.current);
    //if (datePickerRef.current) {
    //console.log(`forwardRefRef: ${datePickerRef.current}`);
    //}
    //});

    //초기값이 null이니 값이 들어있다는 방어기제를 넣어주야됨 !
    //자식 컴퍼넌트에서 함수를 받아와서 실행
    const testClickBt = () => {
        if (datePickerRef.current) {
            datePickerRef.current.click();
        }
    };

    const resetPeriod = useCallback(() => {
        //console.log('reset!!');
        setStartDate(null);
        setEndDate(null);
        //if(datePickerRef.current){
        //console.log(`forwardRefRef: ${datePickerRef.current.style}`);
        //}
    }, []);

    return (
        <div className="datepicker-normal">
            {/* 부모가 자식메소드에 접근할수 있는예제!--ref를 이용해야지만 참조로 접근이 가능 }
            <button type='button' onClick={testClickBt}>부모테스트버튼</button>
            <DatepickerTestBt ref={datePickerRef}/>
            {*/}
            <button type="button" className="reset-period" onClick={resetPeriod}>
                기간 초기화
            </button>
            <DatePicker
                renderCustomHeader={({
                    date,
                    changeYear,
                    changeMonth,
                    decreaseMonth,
                    increaseMonth,
                    prevMonthButtonDisabled,
                    nextMonthButtonDisabled,
                }) => (
                    <div
                        style={{
                            margin: 10,
                            display: 'flex',
                            justifyContent: 'center',
                        }}
                    >
                        <button onClick={decreaseMonth} disabled={prevMonthButtonDisabled}>
                            {'<'}
                        </button>
                        <select
                            value={Commons.getYear(date)}
                            onChange={({ target: { value } }: any) => changeYear(value)}
                        >
                            {years.map((option) => (
                                <option key={option} value={option}>
                                    {option}
                                </option>
                            ))}
                        </select>

                        <select
                            value={months[Commons.getMonth(date)]}
                            onChange={({ target: { value } }) => changeMonth(months.indexOf(value))}
                        >
                            {months.map((option) => (
                                <option key={option} value={option}>
                                    {option}
                                </option>
                            ))}
                        </select>

                        <button onClick={increaseMonth} disabled={nextMonthButtonDisabled}>
                            {'>'}
                        </button>
                    </div>
                )}
                id={'start-date'}
                selected={startDate}
                onChange={(date) => setStartDate(date)}
                selectsStart
                startDate={startDate}
                endDate={endDate}
                dateFormat="yyyy-MM-dd"
                locale="ko"
                customInput={<DatepickerRangeBt />}
            />
            <span>~</span>
            <DatePicker
                renderCustomHeader={({
                    date,
                    changeYear,
                    changeMonth,
                    decreaseMonth,
                    increaseMonth,
                    prevMonthButtonDisabled,
                    nextMonthButtonDisabled,
                }) => (
                    <div
                        style={{
                            margin: 10,
                            display: 'flex',
                            justifyContent: 'center',
                        }}
                    >
                        <button onClick={decreaseMonth} disabled={prevMonthButtonDisabled}>
                            {'<'}
                        </button>
                        <select
                            value={Commons.getYear(date)}
                            onChange={({ target: { value } }: any) => changeYear(value)}
                        >
                            {years.map((option) => (
                                <option key={option} value={option}>
                                    {option}
                                </option>
                            ))}
                        </select>

                        <select
                            value={months[Commons.getMonth(date)]}
                            onChange={({ target: { value } }) => changeMonth(months.indexOf(value))}
                        >
                            {months.map((option) => (
                                <option key={option} value={option}>
                                    {option}
                                </option>
                            ))}
                        </select>

                        <button onClick={increaseMonth} disabled={nextMonthButtonDisabled}>
                            {'>'}
                        </button>
                    </div>
                )}
                id={'end-date'}
                selected={endDate}
                onChange={(date) => setEndDate(date)}
                selectsEnd
                startDate={startDate}
                endDate={endDate}
                minDate={startDate}
                dateFormat="yyyy-MM-dd"
                locale="ko"
                customInput={<DatepickerRangeBt ref={datePickerRef} />}
            />
        </div>
    );
};

export default DatepickerRange;
