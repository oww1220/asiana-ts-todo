import React, { useState, useEffect, useRef } from 'react';
import DatePicker, { registerLocale } from 'react-datepicker';
import ko from 'date-fns/locale/ko';
import 'react-datepicker/dist/react-datepicker.css';

import useLayout from 'hooks/UseLayout';

registerLocale('ko', ko);
type ButtonRef = HTMLButtonElement;

function Datepicker() {
    const datePickerRef = useRef<ButtonRef>(null);
    //const [startDate, setStartDate] = useState(new Date());
    const { dateState, changeDate, getList } = useLayout();

    const ExampleCustomInput = React.forwardRef<ButtonRef, React.ComponentPropsWithoutRef<'button'>>((props, ref) => {
        //console.log('props', props);
        return (
            <button ref={ref} className="example-custom-input" onClick={props.onClick}>
                {props.value}
            </button>
        );
    });

    const date = dateState.getDate();
    const month = dateState.getMonth();
    const year = dateState.getFullYear();

    useEffect(() => {
        console.log('You clicked times');
    }, [date, month, year]);

    return (
        <div className="datepicker_area">
            <DatePicker
                selected={dateState}
                onChange={(date: Date) => {
                    changeDate(date);
                    getList(date);
                }}
                dateFormat="yyyy년MM월dd일"
                customInput={<ExampleCustomInput ref={datePickerRef} />}
                locale="ko"
            />
        </div>
    );
}

export default Datepicker;
