import React, { useEffect, useMemo } from 'react';
import * as Commons from 'lib/Commons';
import useLayout from 'hooks/UseLayout';

function RestrictionModalTop() {
    const { listModalState } = useLayout();
    const listModalCount = useMemo(() => Commons.divisionListModalState(listModalState), [listModalState]);

    return (
        <div className="modal-top-wrap">
            <table cellPadding={0} cellSpacing={0}>
                <colgroup>
                    <col width="10%" />
                    <col width="5%" />
                    <col width="5%" />
                    <col width="5%" />
                    <col width="5%" />
                    <col width="5%" />
                    <col width="5%" />
                    <col width="5%" />
                    <col width="5%" />
                    <col width="5%" />
                    <col width="5%" />
                    <col width="5%" />
                    <col width="5%" />
                    <col width="5%" />
                    <col width="5%" />
                    <col width="5%" />
                    <col width="5%" />
                    <col width="10%" />
                </colgroup>
                <thead>
                    <tr>
                        <th>형식</th>
                        <th colSpan={2}>CMS</th>
                        <th colSpan={2}>이벤트</th>
                        <th colSpan={2}>공지</th>
                        <th colSpan={2}>약관</th>
                        <th colSpan={2}>JSP</th>
                        <th colSpan={2}>HTML</th>
                        <th colSpan={2}>기타</th>
                        <th colSpan={2}>개선</th>
                        <th>합계</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th rowSpan={2}>
                            총<br />
                            <b className="textRed">{listModalState.length}</b>건
                        </th>
                        <td className="grayFill">신규</td>
                        <td>{listModalCount.cmsN}</td>
                        <td className="grayFill">신규</td>
                        <td>{listModalCount.eventN}</td>
                        <td className="grayFill">신규</td>
                        <td>{listModalCount.noticeN}</td>
                        <td className="grayFill">신규</td>
                        <td>{listModalCount.agreeN}</td>
                        <td className="grayFill">신규</td>
                        <td>{listModalCount.jspN}</td>
                        <td className="grayFill">신규</td>
                        <td>{listModalCount.htmlN}</td>
                        <td className="grayFill">신규</td>
                        <td>{listModalCount.etcN}</td>
                        <td className="grayFill">신규</td>
                        <td>{listModalCount.improveN}</td>
                        <td>
                            <b className="textRed">{listModalCount.new}</b>
                        </td>
                    </tr>
                    <tr>
                        <td className="grayFill">수정</td>
                        <td>{listModalCount.cmsM}</td>
                        <td className="grayFill">수정</td>
                        <td>{listModalCount.eventM}</td>
                        <td className="grayFill">수정</td>
                        <td>{listModalCount.noticeM}</td>
                        <td className="grayFill">수정</td>
                        <td>{listModalCount.agreeM}</td>
                        <td className="grayFill">수정</td>
                        <td>{listModalCount.jspM}</td>
                        <td className="grayFill">수정</td>
                        <td>{listModalCount.htmlM}</td>
                        <td className="grayFill">수정</td>
                        <td>{listModalCount.etcM}</td>
                        <td className="grayFill">수정</td>
                        <td>{listModalCount.improveM}</td>
                        <td>
                            <b className="textRed">{listModalCount.modified}</b>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
}

export default RestrictionModalTop;
