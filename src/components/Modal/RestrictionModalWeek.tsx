import React, { useEffect, useMemo } from 'react';
import Popup from 'reactjs-popup';
import * as Commons from 'lib/Commons';
import jquery from 'jquery';
import useLayout from 'hooks/UseLayout';
import RestrictionModalTop from 'components/Modal/RestrictionModalTop';

const $: JQueryStatic = jquery;

function RestrictionModalWeek() {
    const { dateState, range, listModalState, getModalList } = useLayout();

    const restrictionClick = () => {
        const range = Commons.getWeek(dateState);
        getModalList(range);
    };

    //const listModalCount = useMemo(() => Commons.divisionListModalState(listModalState), [listModalState]);

    return (
        <Popup
            trigger={
                <button type="button" className="layer_open_bt">
                    주간 보기
                </button>
            }
            modal
            closeOnDocumentClick
            lockScroll
            className={'restriction'}
            onOpen={restrictionClick}
        >
            {(close) => (
                <div className="layer-in">
                    <div className="pop_cont">
                        <div className="day-report">
                            <p>
                                <span style={{ background: '#f4f4f4', padding: '5px' }}>
                                    <span style={{ color: 'blue' }}>{range[0]}</span> ~{' '}
                                    <span style={{ color: 'blue' }}>{range[6]}</span> 업무
                                </span>
                                {/*}
                                <span style={{ margin: '0 0 0 40px', background: '#f4f4f4', padding: '5px' }}>
                                    신규 {listModalCount.new}건, 수정{listModalCount.modified}건 ={' '}
                                    <span style={{ color: 'red' }}>총건수: {listModalState.length}건</span>
                                </span>
                                {*/}
                            </p>
                            <RestrictionModalTop />
                            <table cellPadding={0} cellSpacing={0} className="report-board">
                                <colgroup>
                                    <col width="8%" />
                                    <col width="8%" />
                                    <col width="8%" />
                                    <col width="auto" />
                                    <col width="20%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th>구분</th>
                                        <th>형태</th>
                                        <th>디바이스</th>
                                        <th>업무 타이틀</th>
                                        <th>언어</th>
                                        <th>업무 시작</th>
                                        <th>업무 종료</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <>
                                        {listModalState.length === 0 ? (
                                            <tr>
                                                <td colSpan={7}>리스트가 없습니다.</td>
                                            </tr>
                                        ) : (
                                            listModalState.map((item, idx) => {
                                                return (
                                                    <tr key={item.id}>
                                                        <td>{item.sort}</td>
                                                        <td>{item.catagory}</td>
                                                        <td>{item.lang}</td>
                                                        <td>
                                                            <p>{item.name}</p>
                                                        </td>
                                                        <td style={{ textAlign: 'left' }}>
                                                            <ul className="langs-links">
                                                                {item.koUrl && (
                                                                    <li>
                                                                        <a href={item.koUrl} target={'_blank'}>
                                                                            <button data-langs-links="ko">KO</button>
                                                                        </a>
                                                                    </li>
                                                                )}
                                                                {item.viUrl && (
                                                                    <li>
                                                                        <a href={item.viUrl} target={'_blank'}>
                                                                            <button data-langs-links="vi">VI</button>
                                                                        </a>
                                                                    </li>
                                                                )}
                                                                {item.esUrl && (
                                                                    <li>
                                                                        <a href={item.esUrl} target={'_blank'}>
                                                                            <button data-langs-links="es">ES</button>
                                                                        </a>
                                                                    </li>
                                                                )}
                                                                {item.enUrl && (
                                                                    <li>
                                                                        <a href={item.enUrl} target={'_blank'}>
                                                                            <button data-langs-links="en">EN</button>
                                                                        </a>
                                                                    </li>
                                                                )}
                                                                {item.jaUrl && (
                                                                    <li>
                                                                        <a href={item.jaUrl} target={'_blank'}>
                                                                            <button data-langs-links="ja">JA</button>
                                                                        </a>
                                                                    </li>
                                                                )}
                                                                {item.chUrl && (
                                                                    <li>
                                                                        <a href={item.chUrl} target={'_blank'}>
                                                                            <button data-langs-links="ch">CH</button>
                                                                        </a>
                                                                    </li>
                                                                )}
                                                                {item.zhUrl && (
                                                                    <li>
                                                                        <a href={item.zhUrl} target={'_blank'}>
                                                                            <button data-langs-links="zh">ZH</button>
                                                                        </a>
                                                                    </li>
                                                                )}
                                                                {item.ruUrl && (
                                                                    <li>
                                                                        <a href={item.ruUrl} target={'_blank'}>
                                                                            <button data-langs-links="ru">RU</button>
                                                                        </a>
                                                                    </li>
                                                                )}
                                                                {item.deUrl && (
                                                                    <li>
                                                                        <a href={item.deUrl} target={'_blank'}>
                                                                            <button data-langs-links="de">DE</button>
                                                                        </a>
                                                                    </li>
                                                                )}
                                                                {item.frUrl && (
                                                                    <li>
                                                                        <a href={item.frUrl} target={'_blank'}>
                                                                            <button data-langs-links="fr">FR</button>
                                                                        </a>
                                                                    </li>
                                                                )}
                                                            </ul>
                                                        </td>
                                                        <td>{item.date}</td>
                                                        <td>{item.chdate}</td>
                                                    </tr>
                                                );
                                            })
                                        )}
                                    </>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <button
                        type="button"
                        className="close"
                        onClick={() => {
                            close();
                        }}
                    >
                        닫기
                    </button>
                </div>
            )}
        </Popup>
    );
}

export default RestrictionModalWeek;
