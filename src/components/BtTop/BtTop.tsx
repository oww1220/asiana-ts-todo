import React, { useEffect } from 'react';
import jquery from 'jquery';

const $: JQueryStatic = jquery;

function BtTop() {
    useEffect(() => {
        $('.bt-top').on('click', function () {
            $('html, body').animate({ scrollTop: 0 }, 500);
        });
        return () => {
            $('.bt-top').off('click');
        };
    }, []);

    useEffect(() => {
        $(window).on('scroll.top', function () {
            const _tarGo = $('.bt-top');
            const scroll = $(window).scrollTop();
            if (!scroll) return;
            if (scroll > 40) {
                _tarGo.fadeIn();
            } else {
                _tarGo.fadeOut();
            }
        });
        return () => {
            $(window).off('scroll.top');
        };
    }, []);

    return <div className="bt-top">TOP</div>;
}

export default BtTop;
