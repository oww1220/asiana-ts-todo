import React, { useCallback, useEffect, useState } from 'react';
import styled from 'styled-components';

const ButtonHoverWrapper = styled.button`
    display: inline-block;
    background-color: transparent;
    padding: 0;
    border: 0;
    position: relative;
    a {
        margin-left: 5px;
    }
    .hoverTooltip {
        position: absolute;
        width: 270px;
        background-color: #fff;
        border: 1px solid #000;
        text-align: left;
        padding: 10px;
        > div {
            font-size: 13px;
        }
    }
`;

interface IButtonHover {
    children: JSX.Element;
}

const ButtonHover = ({ children }: IButtonHover) => {
    //console.log('children', children);
    const [hoverState, setHoverState] = useState(false);

    const handleMouseEnter = useCallback(() => {
        setHoverState(true);
    }, []);

    const handleMouseLeave = useCallback(() => {
        setHoverState(false);
    }, []);

    useEffect(() => {}, []);

    return (
        <ButtonHoverWrapper className="buttonHover" onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave}>
            {children}
            {hoverState && (
                <div className="hoverTooltip">
                    <div>
                        [@Rule Settings] : Nothing
                        <br />
                        <br />
                        [Style Rule Settings]
                        <br />
                        - After selector(s) insert : Space
                        <br />
                        - After open-brace insert : Nothing
                        <br />
                        - Before close-brace insert : Nothing
                        <br />
                        - After close-brace insert : NewLine
                        <br />
                        - Indent Sub-Rules With : Space
                        <br />
                        <br />
                        [Style Property Settings]
                        <br />
                        - Before property names insert : Nothing
                        <br />
                        - Before colon insert: Nothing
                        <br />
                        - After colon insert : Nothing
                        <br />
                        - After semicolon insert : Nothing
                        <br />
                        <br />
                        [Alphabetizing Feature]
                        <br />
                        - Alphabetize @Rules - 체크
                        <br />
                        <br />
                        De-Capitalization Feature
                        <br />- Don't Alter Case - 체크
                    </div>
                </div>
            )}
        </ButtonHoverWrapper>
    );
};

export default ButtonHover;
