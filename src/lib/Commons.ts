import * as Interface from 'interfaces/Interfaces';
import jquery from 'jquery';
const $: JQueryStatic = jquery;

/*하이라이트*/
export const textHighlight = function (val: string) {
    let search = val;
    let tg = $('#container .task-board');
    tg.find('.chk').each(function (idx, item) {
        //console.log(idx, item);
        $(this).html(
            $(this)
                .html()
                .replace(/<span class="highLight".*?>(.*?)/gi, '')
                .replace(/<\/span>/gi, ''),
        );
        let highLight = $(this).find('.highLight').length;
        let tgL = $(this).text();
        let regex = new RegExp(search, 'gi');
        let result: any = tgL.match(regex);
        let info = tgL.search(regex);
        if (highLight == 0 && info != -1) {
            $(this).html(
                $(this)
                    .html()
                    .replace(regex, "<span class='highLight'>" + result[0] + '</span>'),
            );
        }
    });
};

/*날짜 및 요일 반환 함수*/
export const dateCheck = function (target?: any) {
    //console.log(target);
    let data = [];
    let dt = target ? new Date(target) : new Date();
    let month = '' + (dt.getMonth() + 1);
    let day = '' + dt.getDate();
    let year = dt.getFullYear();
    month = month.length < 2 ? '0' + month : month;
    day = day.length < 2 ? '0' + day : day;
    let dateVal = year + '-' + month + '-' + day;
    let week = ['일', '월', '화', '수', '목', '금', '토'];
    let today = dt.getDay();
    let todayLabel = week[today];
    data.push(dateVal);
    data.push(todayLabel);
    return data;
};

/*월 반환 함수*/
export const dateCheckMonth = function (target?: any) {
    //console.log(target);
    let dt = target ? new Date(target) : new Date();
    let month = '' + (dt.getMonth() + 1);
    let day = '' + dt.getDate();
    let year = dt.getFullYear();
    month = month.length < 2 ? '0' + month : month;
    day = day.length < 2 ? '0' + day : day;
    return year + '-' + month;
};

/*대상날짜 주, 시작일 마감일 반환*/
export const getWeek = function (target: Date, start?: number) {
    //Calcing the starting point
    start = start || 0;
    let today = new Date(target);
    let day = today.getDay() - start || 7;
    let date = today.getDate() - day;

    let StartDate, StartDate2, StartDate3, StartDate4, StartDate5, StartDate6, StartDate7;
    // Grabbing Start/End Dates
    //console.log(day, date);

    StartDate = dateCheck(new Date(today.setDate(date + 1)))[0];
    StartDate2 = dateCheck(new Date(StartDate).valueOf() + 24 * 60 * 60 * 1000)[0];
    StartDate3 = dateCheck(new Date(StartDate2).valueOf() + 24 * 60 * 60 * 1000)[0];
    StartDate4 = dateCheck(new Date(StartDate3).valueOf() + 24 * 60 * 60 * 1000)[0];
    StartDate5 = dateCheck(new Date(StartDate4).valueOf() + 24 * 60 * 60 * 1000)[0];
    StartDate6 = dateCheck(new Date(StartDate5).valueOf() + 24 * 60 * 60 * 1000)[0];
    StartDate7 = dateCheck(new Date(StartDate6).valueOf() + 24 * 60 * 60 * 1000)[0];

    return [StartDate, StartDate2, StartDate3, StartDate4, StartDate5, StartDate6, StartDate7];
};

/*날짜 기간 반환 함수*/
export const getDateRange = function (startDate: string, endDate: string, listDate: string[]) {
    let dateMove = new Date(startDate);
    let strDate = startDate;
    if (startDate == endDate) {
        let strDate = dateMove.toISOString().slice(0, 10);
        listDate.push(strDate);
    } else {
        while (strDate < endDate) {
            let strDate = dateMove.toISOString().slice(0, 10);
            listDate.push(strDate);
            dateMove.setDate(dateMove.getDate() + 1);
        }
    }
    return listDate;
};

/*요일 반환 함수*/
export const getInputDayLabel = function (val: string) {
    let today = null;
    let week = new Array('일', '월', '화', '수', '목', '금', '토');
    today = new Date(val).getDay();
    let todayLabel = week[today];
    return todayLabel;
};

/*신규,수정 건수 나누어주는 함수*/
export const divisionListModalState = function (listModalState: Interface.ListState[]) {
    const obj = {
        new: 0,
        modified: 0,
        cmsN: 0,
        cmsM: 0,
        eventN: 0,
        eventM: 0,
        noticeN: 0,
        noticeM: 0,
        agreeN: 0,
        agreeM: 0,
        jspN: 0,
        jspM: 0,
        htmlN: 0,
        htmlM: 0,
        etcN: 0,
        etcM: 0,
        improveN: 0,
        improveM: 0,
    };
    //console.log(listModalState);
    listModalState.forEach((value, index) => {
        if (value.sort === '신규') {
            obj.new = ++obj.new;
            if (value.catagory === 'CMS') obj.cmsN = ++obj.cmsN;
            if (value.catagory === '이벤트') obj.eventN = ++obj.eventN;
            if (value.catagory === '공지') obj.noticeN = ++obj.noticeN;
            if (value.catagory === '약관') obj.agreeN = ++obj.agreeN;
            if (value.catagory === 'JSP') obj.jspN = ++obj.jspN;
            if (value.catagory === 'HTML') obj.htmlN = ++obj.htmlN;
            if (value.catagory === '기타 ') obj.etcN = ++obj.etcN;
            if (value.catagory === '개선') obj.improveN = ++obj.improveN;
        } else if (value.sort === '수정') {
            obj.modified = ++obj.modified;
            if (value.catagory === 'CMS') obj.cmsM = ++obj.cmsM;
            if (value.catagory === '이벤트') obj.eventM = ++obj.eventM;
            if (value.catagory === '공지') obj.noticeM = ++obj.noticeM;
            if (value.catagory === '약관') obj.agreeM = ++obj.agreeM;
            if (value.catagory === 'JSP') obj.jspM = ++obj.jspM;
            if (value.catagory === 'HTML') obj.htmlM = ++obj.htmlM;
            if (value.catagory === '기타 ') obj.etcM = ++obj.etcM;
            if (value.catagory === '개선') obj.improveM = ++obj.improveM;
        }
    });
    //console.log(obj);

    return obj;
};

/*url 생성함수*/
export const creatStringUrl = (values: string, cid: string, catagory: string, useDevice: string) => {
    let url = '';
    const langage = values.toLowerCase();
    const cidVal = !cid ? 'CID' : cid;
    const useDeviceVal = useDevice === 'PC/MO' ? 'pc' : String(useDevice).toLowerCase();
    const textArea = $(`.links-langs-input .${values} textarea`);

    if (catagory === 'CMS' || catagory === '이벤트' || catagory === '공지' || catagory === '약관') {
        let urlCatagory = '',
            urlLinkCatagory = '';
        switch (catagory) {
            case 'CMS':
                urlCatagory = 'cms';
                urlLinkCatagory = 'cms';
                break;
            case '이벤트':
                urlCatagory = 'event';
                urlLinkCatagory = 'event';
                break;
            case '공지':
                urlCatagory = 'notice';
                urlLinkCatagory = 'news';
                break;
            case '약관':
                urlCatagory = 'terms';
                urlLinkCatagory = 'trems';
                break;
        }
        url = `http://localhost/asiana_manage/contents/${urlCatagory}/${cidVal}_${urlLinkCatagory}_${langage}_${useDeviceVal}.php`;
    }
    if (!textArea.hasClass('on')) {
        textArea.val(url);
    }
};

//레인지 함수
export const range = (start: number, end: number) => {
    let arr = [];
    let length = end - start;
    for (let i = 0; i <= length; i++) {
        arr[i] = start;
        start++;
    }
    return arr;
};

//년, 월, 반환
export const getYear = (date: Date) => date.getFullYear();
export const getMonth = (date: Date) => date.getMonth();
