import axios from 'axios';
import * as Interface from 'interfaces/Interfaces';
import * as Commons from 'lib/Commons';

const BaseURL = 'http://local.asianadb:5001';

export const insertList = (data: Interface.ListState) => {
    return axios({
        method: 'post',
        url: `${BaseURL}/lists/`,
        data: data,
    });
};

export const updateList = (data: Interface.ListState, id: string) => {
    //console.log(data, id);
    return axios({
        method: 'put',
        url: `${BaseURL}/lists/${id}`,
        data: data,
    });
};

export const getListDefault = () => {
    return axios({
        method: 'get',
        url: `${BaseURL}/lists?state_ne=end&_sort=date,catagory&_order=desc,desc`,
    });
};

export const getList = (date: Date) => {
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();
    const val = `${year}-${month < 10 ? '0' + month : month}-${day < 10 ? '0' + day : day}`;
    console.log(val);
    return axios({
        method: 'get',
        url: `${BaseURL}/lists?date_like=${val}&_sort=date,catagory&_order=desc,desc`,
    });
};

export const getListItem = (id: string) => {
    return axios({
        method: 'get',
        url: `${BaseURL}/lists?id=${id}`,
    });
};

export const getListModalItem = (range: string[]) => {
    return axios({
        method: 'get',
        url: `${BaseURL}/lists?date_like=${range[0]}&date_like=${range[1]}&date_like=${range[2]}&date_like=${range[3]}&date_like=${range[4]}&date_like=${range[5]}&date_like=${range[6]}`,
    });
};

export const getListModalItemMonth = (date: Date) => {
    //const year = date.getFullYear();
    //const month = date.getMonth() + 1;
    //const val = `${year}-${month < 10 ? '0' + month : month}`;
    const val = Commons.dateCheckMonth(date);
    console.log(val);
    return axios({
        method: 'get',
        url: `${BaseURL}/lists?date_like=${val}&_sort=date&_order=ASC`,
    });
};

export const searchListItem = (
    txt: string,
    start: number,
    end: number,
    startDate: Date | null,
    endDate: Date | null,
) => {
    let RestUrl = '';
    if (!startDate || !endDate) {
        RestUrl = `${BaseURL}/lists?q=${txt}&_start=${start}&_end=${end}&_sort=date,catagory&_order=desc,desc`;
    } else {
        RestUrl = `${BaseURL}/lists?q=${txt}&chdate_gte=${startDate}&chdate_lte=${endDate}&_start=${start}&_end=${end}&_sort=chdate,catagory&_order=desc,desc`;
    }
    //console.log('axios:', txt, start, end, startDate, endDate);
    return axios({
        method: 'get',
        url: RestUrl,
    });
};
