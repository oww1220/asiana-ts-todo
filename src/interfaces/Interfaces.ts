/*반복 인터페이스 정의*/
export interface ListState {
    date: string;
    chdate: string;
    day: string;
    cid: string;
    sort: string;
    name: string;
    catagory: string;
    lang: string;
    koUrl: string;
    viUrl: string;
    esUrl: string;
    enUrl: string;
    jaUrl: string;
    chUrl: string;
    zhUrl: string;
    ruUrl: string;
    deUrl: string;
    frUrl: string;
    state: string;
    cording: string;
    request: string;
    etc: string;
    id: number;
    chk: string[];
    chkall?: boolean;
}

export interface LangsLink {
    KO: boolean;
    VI: boolean;
    ES: boolean;
    EN: boolean;
    JA: boolean;
    CH: boolean;
    ZH: boolean;
    RU: boolean;
    DE: boolean;
    FR: boolean;
}

export interface IClassNames {
    [className: string]: string;
}

export interface IErrors {
    /* The validation error messages for each field (key is the field name */
    [key: string]: string;
}
