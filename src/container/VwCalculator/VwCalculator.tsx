import React, { useState, useEffect } from 'react';
import styled, { createGlobalStyle } from 'styled-components';
import jquery from 'jquery';
import useReactRouter from 'use-react-router';

const $: JQueryStatic = jquery;

const VwCalculatorWrapper = styled.div`
    h1 {
        font-family: 'news-gothic-std', sans-serif;
        text-align: center;
        font-size: 51px;
        margin-bottom: 61px;
        margin-top: 0;
    }
    .field {
        font-size: 0;
        margin-bottom: 3vw;
    }
    .field label {
        width: 50%;
        display: inline-block;
        vertical-align: middle;
        text-align: left;
        text-transhtmlform: uppercase;
        font-size: 3vw;
        padding-bottom: 0px;
    }
    input,
    select {
        -webkit-appearance: none;
        -moz-appearance: none;
        border: 0px solid #3f424c;
        width: 50%;
        margin-bottom: 0;
        height: 7vw;
        float: none;
        clear: both;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        display: inline-block;
        vertical-align: middle;
        letter-spacing: 0.6vw;
        line-height: normal;
        border-radius: 0px;
        background: #fff;
        color: #7c7c7c;
        font-size: 2vw;
        font-family: 'news-gothic-std', sans-serif;
        text-indent: 0;
        text-transhtmlform: lowercase;
        padding: 0 2.6vw 0 2.6vw;
        outline: none;
    }
    input::-webkit-input-placeholder {
        color: #7c7c7c;
    }

    input:-moz-placeholder {
        /* Firefox 18- */
        color: #7c7c7c;
        opacity: 1;
    }

    input::-moz-placeholder {
        /* Firefox 19+ */
        color: #7c7c7c;
        opacity: 1;
    }

    input:-ms-input-placeholder {
        color: #7c7c7c;
    }
    input:-webkit-autofill,
    input:-webkit-autofill:focus {
        -webkit-box-shadow: 0px 0px 0px 10px #fff inset;
        background: #fff !important;
        -webkit-text-fill-color: #7c7c7c;
    }

    div.button {
        text-align: center;
    }
    div.button a {
        background: #9a297b;
        color: #ffffff;
        text-transhtmlform: uppercase;
        letter-spacing: 2px;
        padding: 13px 31px;
        display: inline-block;
        text-decoration: none;
        font-size: 14px;
    }
    div.button a:hover {
        background: #7f7f7f;
    }
    div.output {
        margin-top: 100px;
    }
    .calculator {
        position: fixed;
        right: 0;
        top: 50%;
        width: 104px;
        /*height: 50px;*/
        text-align: center;

        justify-content: center;
        display: flex;
        flex-direction: column;
        cursor: pointer;
    }
    .calculator p {
        margin: 0;
    }
    .calculator a {
        color: #fff;
        text-decoration: none;
        margin: 10px 0px;
        background: #9a297b;
        width: 100%;
        padding: 5px;
    }
    div.output label {
        background: none #42476a;
        width: 100%;
        display: block;
        margin: 20px 0;
        text-align: center;
        font-size: 3vw;
        padding: 20px 0;
        color: #fff;
    }
    @media only screen and (min-width: 768px) {
        body {
            max-width: 60%;
        }

        .field label {
            font-size: 1.7vw;
        }

        input,
        select {
            font-size: 1.25vw;
            height: 3.54vw;
            padding-left: 0.9vw;
            padding-right: 0.9vw;
            letter-spacing: 0.2vw;
        }
    }
    @media screen and (max-width: 768px) {
        body {
            max-width: 81%;
        }
        .calculator {
            display: none;
        }
        .field {
            display: flex;
            flex-direction: column;
            width: 100%;
            justify-content: center;
            align-items: center;
        }
        .field label {
            font-size: 17px;
            width: 100%;
            padding-bottom: 8px;
            text-align: center;
        }
        input,
        select {
            width: 100%;
            margin-bottom: 19px;
            height: 38px;
            font-size: 15px;
        }
        div.output label {
            font-size: 19px;
        }
        h1 {
            font-size: 36px;
            margin-bottom: 34px;
        }
    }
`;
const GlobalStyle = createGlobalStyle`
body {
    margin: 0 auto;
    padding: 10% 0;
    font-style: normal;
    font-family: "news-gothic-std", sans-serif;
    font-weight: 400;
    color: #fff;
    max-width: 80%;
    background-color: #DBC1B3;
    background-image: -webkit-gradient(linear, left top, right top, from(#DBC1B3 ), to(#7f7f7f));
    background-image: -webkit-linear-gradient(left, #DBC1B3 , #7f7f7f);
    background-image: -moz-linear-gradient(left, #DBC1B3 , #7f7f7f);
    background-image: -o-linear-gradient(left, #DBC1B3 , #7f7f7f);
    background-image: linear-gradient(to right, #DBC1B3 , #7f7f7f);
}
`;

function VwCalculator() {
    const { history, location, match } = useReactRouter();
    //console.log(location.pathname);

    useEffect(() => {
        $('.button a').click(function () {
            const pxValue: any = $('#pixel').val();
            const vpwidth: any = $('#vpwidth').val();
            const calcValue: any = pxValue / vpwidth;
            let vwValue: any = parseFloat(calcValue) * 100;
            vwValue = vwValue + 'vw';
            $('.output label').text(vwValue);
            $('.output').show();
        });

        return () => {
            $('.button a').off('click');
        };
    });

    return (
        <>
            <GlobalStyle />
            <VwCalculatorWrapper>
                <h1>VW Calculator</h1>
                <div className="field pixel">
                    <label htmlFor="pixel">Enter Your Pixel Unit</label>
                    <input
                        id="pixel"
                        name="pixel"
                        type="number"
                        min="0"
                        className="m-pixel"
                        placeholder="enter your pixel unit"
                    />
                </div>
                <div className="field pixel">
                    <label htmlFor="vpwidth">Enter Your viewport width</label>
                    <input
                        id="vpwidth"
                        name="vpwidth"
                        type="number"
                        min="0"
                        className="m-vpwidth"
                        placeholder="enter your viewport width"
                    />
                </div>
                <div className="button">
                    <a href={void 0}>Click to get vw unit</a>
                </div>
                <div className="output">
                    <label></label>
                </div>

                <div className="seo_text">
                    <h2>PX to VW calculator htmlFor Front-end developer</h2>
                </div>
            </VwCalculatorWrapper>
        </>
    );
}

export default VwCalculator;
