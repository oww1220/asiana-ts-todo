import React, { useState, useEffect, useCallback } from 'react';
import useLayout from 'hooks/UseLayout';
//import Loading from 'components/Loading/Loading';

/*인터페이스및 타입*/
export type StateSetteDispatch = React.Dispatch<React.SetStateAction<any>>;
export interface DataView1 {
    titleCount: number;
    contentsCount: number;
    title: string;
    contents: string;
}
export interface StateSetter {
    stateSetter: StateSetteDispatch;
    data: DataView1;
    title: any;
    contents: any;
    addTitleCount: (stateSetter: StateSetteDispatch) => void;
    addContentsCount: (stateSetter: StateSetteDispatch) => void;
}

/*함수*/
export const setState = (origin: {}, stateSetter: StateSetteDispatch) => {
    return stateSetter(origin);
};
export function useStateSetter<T extends StateSetter, R>(factory: () => T): T {
    const [g, s] = useState<T>(factory);
    g.stateSetter = s;
    return g;
}

export class View1Model implements StateSetter {
    //커스텀 훅
    private static FACTORY = () => new View1Model();
    static use = () => useStateSetter(View1Model.FACTORY);
    //state갱신 setter
    stateSetter: any;
    //실데이터
    data: any;

    constructor(data?: DataView1) {
        //data가 넘어온 경우는 할당하고 아니면 생성
        this.data = data ?? {
            titleCount: 1,
            contentsCount: 1,
            title: '테스트',
            contents: '본문'
        };
    }

    //title용 게터
    get title() {
        return `${this.data.title}${this.data.titleCount}`;
    }

    //카운트를 올리는 경우
    addTitleCount = (stateSetter: StateSetteDispatch) => {
        this.data.titleCount++;
        console.log(new View1Model(this.data));
        //여기서 state를 갱신한다.
        setState(new View1Model(this.data), stateSetter);
    };

    //상동
    get contents() {
        return `${this.data.contents}${this.data.contentsCount}`;
    }

    addContentsCount = (stateSetter: StateSetteDispatch) => {
        this.data.contentsCount++;
        setState(new View1Model(this.data), stateSetter);
        //setState(new View1Model(this.data));
    };
}

export class View1Command {
    private static factory = () => new View1Command();
    static use = () => {
        const [command] = useState(View1Command.factory);
        command.model = View1Model.use();
        return command;
    };
    model: any;

    h2Click = (model: StateSetter) => {
        console.log('h2Click');
        model.addTitleCount(model.stateSetter);
    };

    divClick = (model: StateSetter) => {
        console.log('divClick');
        model.addContentsCount(model.stateSetter);
    };
}

const sequence = (arr: any[]) => arr.some((f) => f());
/*
sequence([
    　(_:any)=>console.log(1),
    　(_:any)=>(console.log(2), true),
    　(_:any)=>console.log(3)
    ]);*/

const sequenceP = (arr: any[]) => {
    arr.some((f, idx) => {
        const result = f();
        if (!result) return;
        if (result instanceof Promise)
            result.then(
                (_) => sequence(arr.slice(idx + 1)),
                (_) => _
            );
        return true;
    });
};

/*
sequenceP([
    (_:any)=>console.log(1),
    (_:any)=>new Promise((_, r)=>setTimeout(_=>(console.log(2), r()), 500)),
    (_:any)=>console.log(3)
]);*/
const STOP = {},
    YIELD = {};
const sequenceA = (arr: any[]) => {
    let isHold: boolean; //1
    const self = (_?: any) => {
        if (isHold) throw 'is holded(use then)';
        if (!arr.length) throw 'is ended';
        let promise;
        arr.some((f, idx) => {
            const result = f();
            if (!result) return;
            if (result instanceof Promise) {
                arr = arr.slice(idx + 1);
                isHold = true;
                promise = new Promise((res) => {
                    //2
                    result.then(
                        (_) => {
                            isHold = false;
                            if (arr.length) self();
                            res(undefined);
                        },
                        (r) => {
                            isHold = false;
                            if (r == STOP) arr.length = 0;
                            res(undefined);
                        }
                    );
                });
            } else if (result == STOP) {
                arr.length = 0;
            } else if (result == YIELD) {
                arr = arr.slice(idx + 1);
            }
            return true;
        });
        if (promise) return promise; //3
    };
    return self;
};

const co = sequenceA([
    (_: any) => console.log(11),
    (_: any) => (console.log(22), YIELD),
    (_: any) => new Promise((r) => setTimeout((_) => (console.log(33), r(YIELD)), 500)),
    (_: any) => (console.log(44), STOP),
    (_: any) => console.log(55)
]);

co();
co();

const TestPageObg = {
    titleCount: 0,
    contentsCount: 0
};

function TestPage() {
    console.log('testpage');
    //console.log(View1Model.use());
    //const model = View1Model.use();
    //const {model, h2Click, divClick} = View1Command.use();

    const { loadState, showLoad, hideLoad } = useLayout();

    const [testTitle, setTestTitle] = useState(`타이틀${TestPageObg.titleCount}`);
    const titleAdd = useCallback(() => {
        showLoad();
        setTestTitle(`타이틀${++TestPageObg.titleCount}`);
        setTimeout(() => {
            hideLoad();
        }, 300);
    }, []);

    return (
        <>
            TestPage
            <h2 onClick={titleAdd}>{testTitle}</h2>
            <TestPageDepth />
            {/*}
            <h2 onClick={() => model.addTitleCount(model.stateSetter)}>{model.title}</h2>
            <div onClick={() => model.addContentsCount(model.stateSetter)}>{model.contents}</div>
            <h2 onClick={() => h2Click(model)}>{model.title}</h2>
            <div onClick={() => divClick(model)}>{model.contents}</div>
            {loadState && <Loading />}

            {*/}
        </>
    );
}

function TestPageDepth() {
    console.log('TestPageDepth');
    return (
        <>
            <p>TestPageDepth</p>
        </>
    );
}

const MemoizedTestPageDepth = React.memo(TestPageDepth);

export default TestPage;
