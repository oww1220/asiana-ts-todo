import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import * as Interface from 'interfaces/Interfaces';
import useLayout from 'hooks/UseLayout';
import parse from 'html-react-parser';
import Moment from 'react-moment';

interface TableListProps {
    listState: Interface.ListState[];
}

const TableList = ({ listState = [] }: TableListProps) => {
    const { searchStaus } = useLayout();

    return (
        <>
            {listState.length === 0 && !searchStaus ? (
                <tr>
                    <td colSpan={11}>리스트가 없습니다.</td>
                </tr>
            ) : (
                listState.map((item, idx) => {
                    return (
                        <tr key={item.id} className={`${item.state}`}>
                            <td className="chk">
                                <Moment date={item.date} className="start-date" format="YYYY/MM/DD" />
                            </td>
                            <td className="chk">
                                {item.state === 'end' ? (
                                    <Moment date={item.chdate} className="end-date" format="YYYY/MM/DD" />
                                ) : item.state === 'normal' ? (
                                    <span className="normal-state">작업중</span>
                                ) : (
                                    <span className="caution-state">컨폼대기</span>
                                )}
                            </td>
                            <td className="chk">{item.catagory}</td>
                            <td className="chk">{item.lang}</td>
                            <td className="chk">{item.cid ? `[${item.cid}]` : ''}</td>
                            <td className="conts chk">
                                {/*}
                                {item.cid ? <span style={{ display: 'inline-block' }}>[{item.cid}]</span> : ''}{' '}
                                {*/}
                                {item.name}
                            </td>
                            <td>
                                <ul className="langs-links">
                                    {item.koUrl && (
                                        <li>
                                            <a href={item.koUrl} target={'_blank'} rel="noopener noreferrer">
                                                {/*보안을 위해서 rel="noopener noreferrer" 적용시 window.opener 객체 생성이 안됨*/}
                                                <button data-langs-links="ko">KO</button>
                                            </a>
                                        </li>
                                    )}
                                    {item.viUrl && (
                                        <li>
                                            <a href={item.viUrl} target={'_blank'} rel="noopener noreferrer">
                                                <button data-langs-links="vi">VI</button>
                                            </a>
                                        </li>
                                    )}
                                    {item.esUrl && (
                                        <li>
                                            <a href={item.esUrl} target={'_blank'} rel="noopener noreferrer">
                                                <button data-langs-links="es">ES</button>
                                            </a>
                                        </li>
                                    )}
                                    {item.enUrl && (
                                        <li>
                                            <a href={item.enUrl} target={'_blank'} rel="noopener noreferrer">
                                                <button data-langs-links="en">EN</button>
                                            </a>
                                        </li>
                                    )}
                                    {item.jaUrl && (
                                        <li>
                                            <a href={item.jaUrl} target={'_blank'} rel="noopener noreferrer">
                                                <button data-langs-links="ja">JA</button>
                                            </a>
                                        </li>
                                    )}
                                    {item.chUrl && (
                                        <li>
                                            <a href={item.chUrl} target={'_blank'} rel="noopener noreferrer">
                                                <button data-langs-links="ch">CH</button>
                                            </a>
                                        </li>
                                    )}
                                    {item.zhUrl && (
                                        <li>
                                            <a href={item.zhUrl} target={'_blank'} rel="noopener noreferrer">
                                                <button data-langs-links="zh">ZH</button>
                                            </a>
                                        </li>
                                    )}
                                    {item.ruUrl && (
                                        <li>
                                            <a href={item.ruUrl} target={'_blank'} rel="noopener noreferrer">
                                                <button data-langs-links="ru">RU</button>
                                            </a>
                                        </li>
                                    )}
                                    {item.deUrl && (
                                        <li>
                                            <a href={item.deUrl} target={'_blank'} rel="noopener noreferrer">
                                                <button data-langs-links="de">DE</button>
                                            </a>
                                        </li>
                                    )}
                                    {item.frUrl && (
                                        <li>
                                            <a href={item.frUrl} target={'_blank'} rel="noopener noreferrer">
                                                <button data-langs-links="fr">FR</button>
                                            </a>
                                        </li>
                                    )}
                                </ul>
                            </td>
                            <td className="chk" style={{ textAlign: 'left' }}>
                                {parse(item.etc)}
                            </td>
                            <td className="chk">{item.request}</td>
                            <td className="chk">{item.cording}</td>
                            <td>
                                <Link to={`/Write/${item.id}`}>
                                    <button type={'button'}>수정</button>
                                </Link>
                            </td>
                        </tr>
                    );
                })
            )}
        </>
    );
};

export default TableList;
