import React, { useState, useEffect } from 'react';
import Search from 'container/Home/Search';
import TableList from 'container/Home/TableList';
import useLayout from 'hooks/UseLayout';
import useInfiniteScroll from 'hooks/UseInfiniteScroll';
import * as Commons from 'lib/Commons';
import RestrictionModalWeek from 'components/Modal/RestrictionModalWeek';
import RestrictionModalMonth from 'components/Modal/RestrictionModalMonth';
import jquery from 'jquery';
const $: JQueryStatic = jquery;

const Home = () => {
    const {
        dateState,
        startIdx,
        endIdx,
        listState,
        searchStaus,
        searchEnd,
        changeDate,
        getList,
        getListDefault,
        searchList,
        showLoad,
        hideLoad,
    } = useLayout();

    const { isFetching, setIsFetching } = useInfiniteScroll(fetchMoreListItems);

    function fetchMoreListItems() {
        const txt: any = $('#search').val();
        const startDate: any = $('#start-date').val();
        const endDate: any = $('#end-date').val();
        showLoad();
        setTimeout(() => {
            searchList(txt, startIdx, endIdx, startDate, endDate, Commons.textHighlight);
            setIsFetching(false);
        }, 500);
    }

    useEffect(() => {
        changeDate(new Date());
        getListDefault();
        //getList(new Date());
    }, []);

    return (
        <div className="day-tasks">
            <div className="search-w">
                <div className="board-ex">
                    <i>준비 및 진행중</i>
                    <i>컨폼 대기</i>
                    <i>완료</i>
                </div>
                <div className="report-bt">
                    <RestrictionModalWeek />
                </div>
                <div className="report-bt">
                    <RestrictionModalMonth />
                </div>
                <Search textHighlight={Commons.textHighlight} setIsFetching={setIsFetching} isFetching={isFetching} />
            </div>
            <p>언어(langs)순서 : 한(ko), 베(vi), 스(es), 영(en), 일(ja), 중간(ch), 중번(zh), 러(ru), 독(de), 프(fr)</p>
            <table cellPadding="0" className="task-board">
                <colgroup>
                    <col width="5%" />
                    <col width="5%" />
                    <col width="4%" />
                    <col width="4%" />
                    <col width="8%" />
                    <col width="auto" />
                    <col width="15%" />
                    <col width="14%" />
                    <col width="8%" />
                    <col width="6%" />
                    <col width="4%" />
                </colgroup>
                <thead>
                    <tr>
                        <th rowSpan={2}>작업시작일</th>
                        <th rowSpan={2}>작업종료일</th>
                        <th colSpan={3}>분류</th>
                        <th rowSpan={2}>업무 제목</th>
                        <th rowSpan={2}>파일 경로</th>
                        <th rowSpan={2}>기타</th>
                        <th colSpan={2}>작업자</th>
                        <th rowSpan={2}>수정</th>
                    </tr>
                    <tr>
                        <th>카테고리</th>
                        <th>디바이스</th>
                        <th>CID</th>
                        <th>요청자(기획/디자인/현업)</th>
                        <th>코딩</th>
                    </tr>
                </thead>
                <tbody>
                    <TableList listState={listState} />

                    {searchStaus && isFetching && !searchEnd && (
                        <tr>
                            <td colSpan={11}>불러오는 중...</td>
                        </tr>
                    )}
                    {searchStaus && searchEnd && (
                        <tr>
                            <td colSpan={11}>검색정보가 없습니다.</td>
                        </tr>
                    )}
                </tbody>
            </table>
        </div>
    );
};

export default Home;
