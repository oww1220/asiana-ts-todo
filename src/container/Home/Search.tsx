import React, { useEffect, MouseEvent } from 'react';
import jquery from 'jquery';
import useLayout from 'hooks/UseLayout';
import DatepickerRange from 'components/Input/DatepickerRange';

const $: JQueryStatic = jquery;

interface SearchProps {
    textHighlight(val: string): any;
    setIsFetching(val: boolean): any;
    isFetching: boolean;
}

const Search = ({ textHighlight, setIsFetching, isFetching }: SearchProps) => {
    const { loadState, searchEnd, searchList, searchListReset, searchListEnd } = useLayout();

    const serchList = (e: MouseEvent) => {
        const txt: any = $('#search').val();
        const startDate: any = $('#start-date').val();
        const endDate: any = $('#end-date').val();
        //console.log(startIdx, endIdx);
        console.log('기간!!!!', startDate, endDate, txt);
        setIsFetching(false);
        searchListEnd(false);
        searchListReset();
        searchList(txt, -10, 10, startDate, endDate, textHighlight);
        console.log(txt);
    };

    useEffect(() => {
        const $target = $('#search');
        $target.on('keypress', function (e) {
            if (e.which === 13 && (!isFetching || searchEnd)) {
                $('.search-bt').trigger('click.search');
            }
        });
        return () => {
            console.log(`clear keypress event!!!: target=${$target.get(0).localName}`);
            $target.off('keypress');
        };
    }, []);

    return (
        <div className="search">
            <div className="seach-period">
                <DatepickerRange />
            </div>
            <input type="text" id="search" className="inputE" />
            <button className="search-bt" onClick={serchList}>
                검색
            </button>
        </div>
    );
};

export default Search;
