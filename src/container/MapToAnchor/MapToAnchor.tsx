import React, { useState, useEffect } from 'react';
import styled, { createGlobalStyle } from 'styled-components';
import parse from 'html-react-parser';
import jquery from 'jquery';
import useReactRouter from 'use-react-router';

const $: JQueryStatic = jquery;

const MapToAnchorWrapper = styled.div`
    .header {
        padding: 20px;
        font-size: 20px;
        color: #dddedf;
        font-family: 'NanumGothic';
        background: #263238;
    }
    .wrap {
        overflow: hidden;
    }
    .wrap > div {
        float: left;
        width: 48.5%;
        padding: 1%;
    }
    .wrap > div:first-child {
        padding-right: 0;
    }
    .wrap .txt-area {
        width: 100%;
        background: #fff;
        border-radius: 5px;
        border-right: 1px solid rgba(109, 109, 109, 0.2);
        border-bottom: 2px solid rgba(109, 109, 109, 0.2);
        overflow: hidden;
    }
    .wrap .txt-area textarea {
        width: 96%;
        height: 100%;
        padding: 2%;
        margin-bottom: 0;
        border: none;
        text-align: left;
        resize: vertical;
    }
    .wrap .viewport {
        margin-top: 10px;
    }
    .wrap .viewport img {
        max-width: 100%;
    }
    .wrap .result-side .viewport a {
        border: 1px dashed red;
        background: rgba(255, 255, 0, 0.3);
        box-sizing: border-box;
    }
    .wrap .result-side .txt-area {
        position: relative;
        text-align: right;
    }
    .wrap .result-side .txt-area input[type='radio'] {
        position: absolute;
        top: 0;
        left: -9999px;
    }
    .wrap .result-side .txt-area .option-wrap {
        position: absolute;
        top: 5px;
        right: 20px;
    }
    .wrap .result-side .txt-area input[type='radio']:checked + label {
        color: #dddedf;
        background: #263238;
    }
    .wrap .result-side .txt-area label {
        display: inline-block;
        padding: 5px 10px;
        border: 1px solid #263238;
        background: #fff;
        cursor: pointer;
    }
`;

const GlobalStyle = createGlobalStyle`
html {font-size:20px;}
body {background:#ECEFF2; overflow-y:scroll;}
`;

function MapToAnchor() {
    const { history, location, match } = useReactRouter();
    console.log(location.pathname);

    const [oriValue, setOriValue] = useState('');
    const [chkValue, setChkValue] = useState(localStorage.getItem('resultOption') || 'percent');

    const resultFunc = () => {
        if (oriValue == '') {
            $('.result-side textarea').val('');
            $('.result-side .viewport').html('');
            return;
        }

        const _style =
            '<style type="text/css">' +
            '  .hasmap-wrap {display:block; position:relative;}' +
            '	.hasmap-wrap img {width:100%;}' +
            '	.hasmap-wrap a {position:absolute; display:block; font:0/0 a;background-color: rgba(0,0,0,0);}' +
            '</style>';

        const PromiseArr: any[] = [];

        $('.result-side .viewport').html(_style + oriValue);
        $('.result-side map').each(function (idx) {
            let _thisName = $(this).attr('name'); // map 의 name값
            let _findImg = $('.result-side img[usemap="#' + _thisName + '"]'); // 해당 map을 참조하는 이미지
            $(_findImg).each(function () {
                let _this = $(this);
                let _newImage: any = new Image();

                _newImage.src = _this.attr('src');
                _this.wrap('\n<div class="hasmap-wrap" />'); // wrap 생성
                //console.log("0번");

                PromiseArr[idx] = new Promise(function (resolve, reject) {
                    _newImage.onload = function () {
                        resolve();
                        //console.log("1번");
                    };
                }).then(function () {
                    //console.log("2번");
                    if (chkValue == 'percent') {
                        calPercent(_this, _thisName, _newImage);
                    } else if (chkValue == 'rem') {
                        calRem(_this, _thisName, _newImage);
                    }
                    _this.removeAttr('usemap'); // 이미지에서 usemap값 삭제
                });
            });
        });

        Promise.all(PromiseArr).then(function () {
            //console.log("4번");
            $('.result-side map').remove();
            $('.result-side textarea').val(
                $('.result-side .viewport')
                    .html()
                    .replace(/\t/g, '')
                    .replace(/\n/g, '')
                    .replace('<img', '\n	<img')
                    .replace(/\<a href/g, '\n	<a href')
                    .replace('</div>', '\n</div>')
                    .replace(/\.hasmap/g, '\n	.hasmap')
                    .replace('</style>', '\n</style>\n')
            );
        });
    };

    const calPercent = (_this: any, _thisName: any, _newImage: any) => {
        //console.log("3번");
        $('.result-side .viewport').removeAttr('style');
        $('.result-side [name=' + _thisName + '] area').each(function () {
            let _target: any = $(this);
            let _thisHref = _target.attr('href');
            let _thisAlt = _target.attr('alt');
            let _coordsArray = _target.attr('coords').split(','); // 좌표 배열
            let _calLeft = ((_coordsArray[0] / _newImage.width) * 100).toFixed(4) + '%'; // left값 계산
            let _calTop = ((_coordsArray[1] / _newImage.height) * 100).toFixed(4) + '%'; // top값 계산
            let _calWidth = (((_coordsArray[2] - _coordsArray[0]) / _newImage.width) * 100).toFixed(4) + '%'; // width값 계산
            let _calheight = (((_coordsArray[3] - _coordsArray[1]) / _newImage.height) * 100).toFixed(4) + '%'; // height값 계산

            if (_thisAlt == null || _thisAlt == '') {
                _thisAlt = 'alt없음';
            }
            _this
                .closest('.hasmap-wrap')
                .append(
                    '<a href="' +
                        _thisHref +
                        '" style="' +
                        'left:' +
                        _calLeft +
                        '; top:' +
                        _calTop +
                        '; width:' +
                        _calWidth +
                        '; height:' +
                        _calheight +
                        ';">' +
                        _thisAlt +
                        '</a>'
                ); // a태그 생성
        });
    };

    const calRem = (_this: any, _thisName: any, _newImage: any) => {
        //console.log("3번");
        $('.result-side .viewport').css('width', _newImage.width);
        $('.result-side [name=' + _thisName + '] area').each(function () {
            let _target: any = $(this);
            let _thisHref = _target.attr('href');
            let _thisAlt = _target.attr('alt');
            let _coordsArray = _target.attr('coords').split(','); // 좌표 배열
            let _calLeft = _coordsArray[0] / 20 + 'rem'; // left값 계산
            let _calTop = _coordsArray[1] / 20 + 'rem'; // top값 계산
            let _calWidth = (_coordsArray[2] - _coordsArray[0]) / 20 + 'rem'; // width값 계산
            let _calheight = (_coordsArray[3] - _coordsArray[1]) / 20 + 'rem'; // height값 계산

            if (_thisAlt == null || _thisAlt == '') {
                _thisAlt = 'alt없음';
            }
            _this
                .closest('.hasmap-wrap')
                .append(
                    '<a href="' +
                        _thisHref +
                        '" style="' +
                        'left:' +
                        _calLeft +
                        '; top:' +
                        _calTop +
                        '; width:' +
                        _calWidth +
                        '; height:' +
                        _calheight +
                        ';">' +
                        _thisAlt +
                        '</a>'
                ); // a태그 생성
        });
    };

    const radioChange = (event: any) => {
        //console.log(event.target.value);
        localStorage.setItem('resultOption', event.target.value);
        setChkValue(event.target.value);
    };

    const oriChange = (event: any) => {
        setOriValue(event.target.value);
    };

    useEffect(() => {
        console.log('리렌더링');
        resultFunc();
    }, [oriValue, chkValue]);

    return (
        <>
            <GlobalStyle></GlobalStyle>
            <MapToAnchorWrapper>
                <div className="header">MAP TO ANCHOR</div>
                <div className="wrap">
                    <div className="original-side">
                        <div className="txt-area">
                            <textarea
                                name=""
                                id=""
                                cols={30}
                                rows={10}
                                placeholder="Map 코드 입력"
                                value={oriValue}
                                onChange={oriChange}
                            ></textarea>
                        </div>
                        <div className="viewport">{parse(oriValue)}</div>
                    </div>

                    <div className="result-side">
                        <div className="txt-area">
                            <div className="option-wrap">
                                <input
                                    type="radio"
                                    id="percent"
                                    value="percent"
                                    name="optionRadio"
                                    onChange={radioChange}
                                    checked={chkValue == 'percent'}
                                />
                                <label htmlFor="percent">퍼센트</label>
                                <input
                                    type="radio"
                                    id="rem"
                                    value="rem"
                                    name="optionRadio"
                                    onChange={radioChange}
                                    checked={chkValue == 'rem'}
                                />
                                <label htmlFor="rem">REM</label>
                            </div>
                            <textarea
                                name=""
                                cols={30}
                                rows={10}
                                placeholder="Anchor 코드 출력"
                                readOnly={true}
                            ></textarea>
                        </div>
                        <div className="viewport"></div>
                    </div>
                </div>
            </MapToAnchorWrapper>
        </>
    );
}

export default MapToAnchor;
