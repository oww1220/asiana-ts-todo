import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import useReactRouter from 'use-react-router';
import useWrite from 'hooks/UseWrite';
import useLayout from 'hooks/UseLayout';
import * as Interface from 'interfaces/Interfaces';
import Input from 'components/Input/Input';
import * as Commons from 'lib/Commons';
import DatepickerInput from 'components/Input/DatepickerInput';

import jquery from 'jquery';
const $: JQueryStatic = jquery;

interface MatchParams {
    name: string;
}

function Write() {
    const { history, location, match } = useReactRouter<MatchParams>();
    const {
        listState,
        langsLink,
        defaultState,
        catagorySelect,
        getWrite,
        postWrite,
        putWrite,
        setWriteLink,
        setCatagorySelect,
        getWriteReset,
    } = useWrite();
    const { changeSearch } = useLayout();
    const { setValue, getValues, register, handleSubmit, errors, control, reset } = useForm<Interface.ListState>({
        mode: 'onBlur',
    });

    const paramChk = match.params.name !== undefined;

    const {
        date: paraMdate,
        chdate: paraMchdate,
        day: paraMday,
        cid: paraMcid,
        sort: paraMsort,
        name: paraMname,
        lang: paraMlang,
        catagory: paraMcatagory,
        koUrl: paraMkoUrl,
        viUrl: paraMviUrl,
        esUrl: paraMesUrl,
        enUrl: paraMenUrl,
        jaUrl: paraMjaUrl,
        chUrl: paraMchUrl,
        zhUrl: paraMzhUrl,
        ruUrl: paraMruUrl,
        deUrl: paraMdeUrl,
        frUrl: paraMfrUrl,
        state: paraMstate,
        cording: paraMcording,
        request: paraMrequest,
        etc: paraMetc,
        id: paraMid,
    } = listState[0];

    const onSubmit = (data: Interface.ListState) => {
        delete data.chkall;
        //console.log(data);
        if (paramChk) {
            putWrite(data, match.params.name, history);
        } else {
            postWrite(data, history);
        }
        changeSearch(false);
    };

    const goHome = () => {
        changeSearch(false);
        history.push('/');
    };

    const stateChk = (e: React.ChangeEvent<any>) => {
        //console.log(paraMstate);
        if (paraMstate !== 'end' && paramChk) {
            if (e.target.value === 'end') {
                //console.log(e.target.value);
                setValue('chdate', Commons.dateCheck()[0]);
            } else {
                setValue('chdate', paraMchdate);
            }
        }
    };

    const langsChk = (e: React.ChangeEvent<any>) => {
        //console.log(e.target.value, e.target.checked);
        if (e.target.checked) {
            setWriteLink({
                ...langsLink,
                [e.target.value]: true,
            });
        } else {
            setWriteLink({
                ...langsLink,
                [e.target.value]: false,
            });
        }
    };
    const langsChkall = (e: React.ChangeEvent<any>) => {
        if (e.target.checked) {
            setWriteLink({
                KO: true,
                VI: true,
                ES: true,
                EN: true,
                JA: true,
                CH: true,
                ZH: true,
                RU: true,
                DE: true,
                FR: true,
            });
            setValue('chk', ['KO', 'VI', 'ES', 'EN', 'JA', 'CH', 'ZH', 'RU', 'DE', 'FR']);
        } else {
            setWriteLink({
                KO: false,
                VI: false,
                ES: false,
                EN: false,
                JA: false,
                CH: false,
                ZH: false,
                RU: false,
                DE: false,
                FR: false,
            });
            setValue('chk', []);
        }
    };

    const catagoryChange = (e: React.ChangeEvent<any>) => {
        setCatagorySelect(e.target.value);
    };

    useEffect(() => {
        //$<HTMLInputElement>('input[name="chk"]').prop('checked', false);
        //reset();
        if (paramChk) {
            getWrite(match.params.name, reset);
        } else {
            getWriteReset();
            reset(defaultState, { errors: true });
        }
    }, [match.params.name]);

    useEffect(() => {
        //제이쿼리 대상 돔 타켓을 클로저scope로 잡아놓으야됨
        //그래야지 하단 클리어 콜백 함수 입장에서 이전 상태를 알수 있음..
        const target = $('.link-chk label input');
        const chkAll = $('input[name="chkall"]');
        const catagory = $('input[name="lang"], input[name="catagory"], input[name="cid"]');

        chkAll.on('change', function () {
            target.each((idx, item) => {
                $(item).trigger('change');
            });
        });

        catagory.on('change', function () {
            target.each((idx, item) => {
                const values = String($(item).val());
                const textArea = $(`.links-langs-input .${values} textarea`);
                const cid = String($('input[name="cid"]').val());
                const catagory = String($('input[name="catagory"]:checked').val());
                const useDevice = String($('input[name="lang"]:checked').val());

                if (textArea.length) {
                    //url 링크생성
                    Commons.creatStringUrl(values, cid, catagory, useDevice);
                }
            });
        });

        target.on('change', function () {
            if ($(this).prop('checked')) {
                const values = String($(this).val());
                //const textArea = $(`.links-langs-input .${values} textarea`);
                const cid = String($('input[name="cid"]').val());
                const catagory = String($('input[name="catagory"]:checked').val());
                const useDevice = String($('input[name="lang"]:checked').val());

                //url 링크생성
                Commons.creatStringUrl(values, cid, catagory, useDevice);
            }
        });

        return () => {
            console.log('off!!!');
            chkAll.off('change');
            catagory.off('change');
            target.off('change');
        };
    }, [listState[0].catagory]);

    return (
        <>
            {paramChk ? <h2 className="tit">TASK 수정 ({paraMname})</h2> : <h2 className="tit">TASK 등록 </h2>}
            <div className="write-wrap">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <p className="guide">
                        <em>*</em> 필수입력사항
                    </p>
                    <table>
                        <colgroup>
                            <col width="22%" />
                            <col width="" />
                            <col width="22%" />
                            <col width="" />
                        </colgroup>
                        <tbody>
                            <tr>
                                <th>
                                    작업시작일, 종료일
                                    <br />
                                    (오늘 날짜자동생성)
                                </th>
                                <td>
                                    {/*}
                                    <Input
                                        type={'text'}
                                        name={'date'}
                                        ref={register({
                                            required: '필수입력사항입니다.'
                                        })}
                                        //defaultValue={paramChk ? paraMdate : Commons.dateCheck()[0]}
                                        errors={errors}
                                        readOnly={true}
                                    />
                                    <Input
                                        style={{ display: 'none' }}
                                        type={'text'}
                                        name={'chdate'}
                                        ref={register({
                                            required: '필수입력사항입니다.'
                                        })}
                                        //defaultValue={paramChk ? paraMchdate : Commons.dateCheck()[0]}
                                        errors={errors}
                                    />
                                    {*/}
                                    <ul className="links-langs-input">
                                        <li>
                                            <span style={{ width: '70px' }}>작업시작일 : </span>
                                            <DatepickerInput
                                                control={control}
                                                name={'date'}
                                                reset={reset}
                                                setValue={setValue}
                                                getValues={getValues}
                                                register={register}
                                                target={listState[0].date}
                                            />
                                        </li>
                                        <li>
                                            <span style={{ width: '70px' }}>작업종료일 : </span>
                                            <DatepickerInput
                                                control={control}
                                                name={'chdate'}
                                                reset={reset}
                                                setValue={setValue}
                                                getValues={getValues}
                                                register={register}
                                                target={listState[0].chdate}
                                            />
                                        </li>
                                    </ul>
                                </td>
                                <th style={{ textAlign: 'center' }}>
                                    요일
                                    <br />
                                    (오늘 날짜자동생성)
                                </th>
                                <td>
                                    <Input
                                        type={'text'}
                                        name={'day'}
                                        ref={register({
                                            required: '필수입력사항입니다.',
                                        })}
                                        //defaultValue={paramChk ? paraMday : Commons.dateCheck()[1]}
                                        errors={errors}
                                        readOnly={true}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <th>CID</th>
                                <td colSpan={3}>
                                    <Input
                                        type={'text'}
                                        name={'cid'}
                                        ref={register}
                                        defaultValue={paraMcid}
                                        errors={errors}
                                        placeholder={'CID 등록'}
                                    />
                                    <span className="input-guide" style={{ display: 'block' }}>
                                        ※ 없을 시 미등록
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <em>*</em>업무제목
                                </th>
                                <td colSpan={3}>
                                    <Input
                                        type={'text'}
                                        name={'name'}
                                        ref={register({
                                            required: '필수입력사항입니다.',
                                        })}
                                        //defaultValue={paraMname}
                                        errors={errors}
                                        placeholder={'컨텐츠 타이틀 등록'}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <em>*</em>유형
                                </th>
                                <td colSpan={3}>
                                    <Input
                                        type={'radio'}
                                        name={'sort'}
                                        list={['신규', '수정']}
                                        ref={register({
                                            required: '필수입력사항입니다.',
                                        })}
                                        //defaultValue={paraMsort}
                                        errors={errors}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <em>*</em>사용기기
                                </th>
                                <td colSpan={3}>
                                    <Input
                                        type={'radio'}
                                        name={'lang'}
                                        list={['PC', 'MO', 'PC/MO']}
                                        ref={register({
                                            required: '필수입력사항입니다.',
                                        })}
                                        //defaultValue={paraMlang}
                                        errors={errors}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <em>*</em>형식
                                </th>
                                <td colSpan={3}>
                                    <Input
                                        type={'radio'}
                                        name={'catagory'}
                                        list={['CMS', '이벤트', '공지', '약관', 'JSP', 'HTML', '기타 ', '개선']}
                                        ref={register({
                                            required: '필수입력사항입니다.',
                                        })}
                                        handleClickP={catagoryChange}
                                        //defaultValue={paraMcatagory}
                                        errors={errors}
                                    />
                                    {catagorySelect === 'CMS' && (
                                        <p className="input-guide">
                                            - PC : http://localhost/asiana_manage/contents/cms/CID_cms_언어_pc.php
                                            <br />- MO : http://localhost/asiana_manage/contents/cms/CID_cms_언어_mo.php
                                        </p>
                                    )}
                                    {catagorySelect === '이벤트' && (
                                        <p className="input-guide">
                                            - PC : http://localhost/asiana_manage/contents/event/CID_event_언어_pc.php
                                            <br />- MO :
                                            http://localhost/asiana_manage/contents/event/CID_event_언어_mo.php
                                        </p>
                                    )}
                                    {catagorySelect === '공지' && (
                                        <p className="input-guide">
                                            - PC : http://localhost/asiana_manage/contents/notice/CID_news_언어_pc.php
                                            <br />- MO :
                                            http://localhost/asiana_manage/contents/notice/CID_news_언어_mo.php
                                        </p>
                                    )}
                                    {catagorySelect === '약관' && (
                                        <p className="input-guide">
                                            - PC/MO :
                                            http://localhost/asiana_manage/contents/terms/CID_trems_언어_pc.php
                                        </p>
                                    )}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <em>*</em>이벤트 url등록
                                </th>
                                <td colSpan={3}>
                                    <div>
                                        <label htmlFor={'chkall'}>
                                            <input
                                                type={'checkbox'}
                                                name={'chkall'}
                                                id={'chkall'}
                                                value={'ALL'}
                                                ref={register}
                                                onClick={langsChkall}
                                            />
                                            <span>전체선택/해제</span>
                                        </label>
                                    </div>
                                    <div className="link-chk">
                                        <label htmlFor={'chk1'}>
                                            <input
                                                type={'checkbox'}
                                                name={'chk'}
                                                id={'chk1'}
                                                value={'KO'}
                                                ref={register}
                                                //defaultChecked={langsLink.KO}
                                                onClick={langsChk}
                                            />
                                            <span>KO</span>
                                        </label>
                                        <label htmlFor={'chk2'}>
                                            <input
                                                type={'checkbox'}
                                                name={'chk'}
                                                id={'chk2'}
                                                value={'VI'}
                                                ref={register}
                                                //defaultChecked={langsLink.VI}
                                                onClick={langsChk}
                                            />
                                            <span>VI</span>
                                        </label>
                                        <label htmlFor={'chk3'}>
                                            <input
                                                type={'checkbox'}
                                                name={'chk'}
                                                id={'chk3'}
                                                value={'ES'}
                                                ref={register}
                                                //defaultChecked={langsLink.ES}
                                                onClick={langsChk}
                                            />
                                            <span>ES</span>
                                        </label>
                                        <label htmlFor={'chk4'}>
                                            <input
                                                type={'checkbox'}
                                                name={'chk'}
                                                id={'chk4'}
                                                value={'EN'}
                                                ref={register}
                                                //defaultChecked={langsLink.EN}
                                                onClick={langsChk}
                                            />
                                            <span>EN</span>
                                        </label>
                                        <label htmlFor={'chk5'}>
                                            <input
                                                type={'checkbox'}
                                                name={'chk'}
                                                id={'chk5'}
                                                value={'JA'}
                                                ref={register}
                                                //defaultChecked={langsLink.JA}
                                                onClick={langsChk}
                                            />
                                            <span>JA</span>
                                        </label>
                                        <label htmlFor={'chk6'}>
                                            <input
                                                type={'checkbox'}
                                                name={'chk'}
                                                id={'chk6'}
                                                value={'CH'}
                                                ref={register}
                                                //defaultChecked={langsLink.CH}
                                                onClick={langsChk}
                                            />
                                            <span>CH</span>
                                        </label>
                                        <label htmlFor={'chk7'}>
                                            <input
                                                type={'checkbox'}
                                                name={'chk'}
                                                id={'chk7'}
                                                value={'ZH'}
                                                ref={register}
                                                //defaultChecked={langsLink.ZH}
                                                onClick={langsChk}
                                            />
                                            <span>ZH</span>
                                        </label>
                                        <label htmlFor={'chk8'}>
                                            <input
                                                type={'checkbox'}
                                                name={'chk'}
                                                id={'chk8'}
                                                value={'RU'}
                                                ref={register}
                                                //defaultChecked={langsLink.RU}
                                                onClick={langsChk}
                                            />
                                            <span>RU</span>
                                        </label>
                                        <label htmlFor={'chk9'}>
                                            <input
                                                type={'checkbox'}
                                                name={'chk'}
                                                id={'chk9'}
                                                value={'DE'}
                                                ref={register}
                                                //defaultChecked={langsLink.DE}
                                                onClick={langsChk}
                                            />
                                            <span>DE</span>
                                        </label>
                                        <label htmlFor={'chk10'}>
                                            <input
                                                type={'checkbox'}
                                                name={'chk'}
                                                id={'chk10'}
                                                value={'FR'}
                                                ref={register}
                                                //defaultChecked={langsLink.FR}
                                                onClick={langsChk}
                                            />
                                            <span>FR</span>
                                        </label>
                                    </div>
                                    <ul className="links-langs-input">
                                        {langsLink.KO && (
                                            <li className="KO">
                                                <span>KO : </span>
                                                <Input
                                                    type={'textarea'}
                                                    name={'koUrl'}
                                                    ref={register}
                                                    placeholder={'예:/contents/event/cid_event_lang_device.php'}
                                                    defaultValue={paraMkoUrl}
                                                    errors={errors}
                                                />
                                            </li>
                                        )}
                                        {langsLink.VI && (
                                            <li className="VI">
                                                <span>VI : </span>
                                                <Input
                                                    type={'textarea'}
                                                    name={'viUrl'}
                                                    ref={register}
                                                    placeholder={'예:/contents/event/cid_event_lang_device.php'}
                                                    defaultValue={paraMviUrl}
                                                    errors={errors}
                                                />
                                            </li>
                                        )}
                                        {langsLink.ES && (
                                            <li className="ES">
                                                <span>ES : </span>
                                                <Input
                                                    type={'textarea'}
                                                    name={'esUrl'}
                                                    ref={register}
                                                    placeholder={'예:/contents/event/cid_event_lang_device.php'}
                                                    defaultValue={paraMesUrl}
                                                    errors={errors}
                                                />
                                            </li>
                                        )}
                                        {langsLink.EN && (
                                            <li className="EN">
                                                <span>EN : </span>
                                                <Input
                                                    type={'textarea'}
                                                    name={'enUrl'}
                                                    ref={register}
                                                    placeholder={'예:/contents/event/cid_event_lang_device.php'}
                                                    defaultValue={paraMenUrl}
                                                    errors={errors}
                                                />
                                            </li>
                                        )}
                                        {langsLink.JA && (
                                            <li className="JA">
                                                <span>JA : </span>
                                                <Input
                                                    type={'textarea'}
                                                    name={'jaUrl'}
                                                    ref={register}
                                                    placeholder={'예:/contents/event/cid_event_lang_device.php'}
                                                    defaultValue={paraMjaUrl}
                                                    errors={errors}
                                                />
                                            </li>
                                        )}
                                        {langsLink.CH && (
                                            <li className="CH">
                                                <span>CH : </span>
                                                <Input
                                                    type={'textarea'}
                                                    name={'chUrl'}
                                                    ref={register}
                                                    placeholder={'예:/contents/event/cid_event_lang_device.php'}
                                                    defaultValue={paraMchUrl}
                                                    errors={errors}
                                                />
                                            </li>
                                        )}
                                        {langsLink.ZH && (
                                            <li className="ZH">
                                                <span>ZH : </span>
                                                <Input
                                                    type={'textarea'}
                                                    name={'zhUrl'}
                                                    ref={register}
                                                    placeholder={'예:/contents/event/cid_event_lang_device.php'}
                                                    defaultValue={paraMzhUrl}
                                                    errors={errors}
                                                />
                                            </li>
                                        )}
                                        {langsLink.RU && (
                                            <li className="RU">
                                                <span>RU : </span>
                                                <Input
                                                    type={'textarea'}
                                                    name={'ruUrl'}
                                                    ref={register}
                                                    placeholder={'예:/contents/event/cid_event_lang_device.php'}
                                                    defaultValue={paraMruUrl}
                                                    errors={errors}
                                                />
                                            </li>
                                        )}
                                        {langsLink.DE && (
                                            <li className="DE">
                                                <span>DE : </span>
                                                <Input
                                                    type={'textarea'}
                                                    name={'deUrl'}
                                                    ref={register}
                                                    placeholder={'예:/contents/event/cid_event_lang_device.php'}
                                                    defaultValue={paraMdeUrl}
                                                    errors={errors}
                                                />
                                            </li>
                                        )}
                                        {langsLink.FR && (
                                            <li className="FR">
                                                <span>FR : </span>
                                                <Input
                                                    type={'textarea'}
                                                    name={'frUrl'}
                                                    ref={register}
                                                    placeholder={'예:/contents/event/cid_event_lang_device.php'}
                                                    defaultValue={paraMfrUrl}
                                                    errors={errors}
                                                />
                                            </li>
                                        )}
                                    </ul>

                                    <p className="input-guide">
                                        ※ 일반컨텐츠 : /contents/
                                        <br />
                                        ※ TEST(테스트서버) : https://test.flyasiana.com/C/
                                        <br />※ TEST(테스트서버) : https://testm.flyasiana.com/C/
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <em>*</em>상태
                                </th>
                                <td colSpan={3}>
                                    <Input
                                        type={'radio'}
                                        name={'state'}
                                        list={['normal', 'caution', 'end']}
                                        ref={register({
                                            required: '필수입력사항입니다.',
                                        })}
                                        //defaultValue={paramChk ? paraMstate : 'normal'}
                                        errors={errors}
                                        handleClickP={stateChk}
                                    />
                                    <p className="input-guide">※ 준비 및 진행중:normal, 컨폼 대기:caution, 완료:end </p>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <em>*</em>작업자
                                </th>
                                <td colSpan={3}>
                                    <Input
                                        type={'radio'}
                                        name={'cording'}
                                        list={[
                                            '이현구',
                                            '이창우',
                                            '정진호',
                                            '이현구',
                                            '정우희',
                                            '김세은',
                                            '박수현',
                                            '박소혜',
                                            '기타',
                                        ]}
                                        ref={register({
                                            required: '필수입력사항입니다.',
                                        })}
                                        //defaultValue={paraMcording}
                                        errors={errors}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <em>*</em>요청자
                                </th>
                                <td colSpan={3}>
                                    <Input
                                        type={'radio'}
                                        name={'request'}
                                        list={['김미선', '김지우', '이상호', '장준석', '기타']}
                                        ref={register({
                                            required: '필수입력사항입니다.',
                                        })}
                                        //defaultValue={paraMrequest}
                                        errors={errors}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <th>기타(태그로 파싱됨)</th>
                                <td colSpan={3}>
                                    <Input
                                        type={'textarea'}
                                        name={'etc'}
                                        ref={register}
                                        placeholder={'작업내용에 필요한 내용 있을시 내용 추가'}
                                        //defaultValue={paraMetc}
                                        errors={errors}
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <div className="btn-wrap">
                        {paramChk ? (
                            <input type="submit" value="수정하기" className="btn" />
                        ) : (
                            <input type="submit" value="글 저장하기" className="btn" />
                        )}
                        <input type="button" value="되돌아가기" onClick={goHome} className="btn" />
                    </div>
                </form>
            </div>
        </>
    );
}

export default Write;
