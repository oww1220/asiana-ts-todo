import React, { useEffect } from "react";
import styled from "styled-components";
import jquery from "jquery";

const $: JQueryStatic = jquery;

const UnEscapeWrapper = styled.div`
        textarea {
            width: 100%;
            height: 120px;
            border-color: black;
            border-width: 1px;
            margin-bottom: 30px;
        }

        div.box {
            margin:10px;
            width: 100%;
            padding: 10px;
            border: 3px dashed #000;
            box-sizing: border-box;
        }

        .btn_area {
            text-align: center;
        }

        button {
            /*float: right*/
        }
`;



function UnEscape(){

    useEffect(() => {
        $('#btnUnescape').click(function(){
            var safe: any = $('#txtSafe').val();
            $('#dvUnsafe').text($('<div />').html(safe).html());
    
        });
    
        $('#dvUnsafe, dvSafe').focusin(function(){$(this).select()});

        return () =>{
			$('#btnUnescape').off("click");
		}
    });


	return (
        <>
            <UnEscapeWrapper>

            <div className="box">
                <h4>escape to unescape</h4>
                <textarea id="dvUnsafe" disabled={true}></textarea>
                <br/>

                <textarea id="txtSafe" ></textarea>
                <div className="btn_area">
                    <button id="btnUnescape">UnEscape</button>
                </div>

            </div>
            
            </UnEscapeWrapper>

        </>
	)
}

export default UnEscape;
