import React, { useState, useEffect } from 'react';
import styled, { createGlobalStyle } from 'styled-components';
import jquery from 'jquery';
import useReactRouter from 'use-react-router';

const $: JQueryStatic = jquery;

const SortSourceWrapper = styled.div`
    .btn {
        display: inline-block;
        padding: 6px 12px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
    }
    .btn-primary {
        color: #fff;
        background-color: #337ab7;
        border-color: #2e6da4;
    }
    .container {
        width: 770px;
        margin: 50px auto;
        font-size: 15px;
    }
    pre {
        display: block;
        padding: 9.5px;
        margin: 0 0 10px;
        font-size: 13px;
        line-height: 1.42857143;
        color: #333;
        word-break: break-all;
        word-wrap: break-word;
        background-color: #f5f5f5;
        border: 1px solid #ccc;
        border-radius: 4px;
    }
    .form-control {
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        -webkit-transition: border-color ease-in-out 0.15s, -webkit-box-shadow ease-in-out 0.15s;
        -o-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
        transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
    }
    textarea.form-control {
        height: auto;
    }
    h1 b {
        font-size: 36px;
    }
    #title {
        font-size: 15px;
        margin-bottom: 30px;
    }
    .form-group {
        margin-bottom: 15px;
    }
    .form-group label {
        font-size: 15px;
    }
`;

declare global {
    interface Window {
        html_beautify: any;
        js_beautify: any;
    }
}

function SortSource() {
    const { history, location, match } = useReactRouter();
    //console.log(location.pathname);

    useEffect(() => {
        // 변환 버튼
        $('#convertBtn').on('click', function () {
            const source: any = $('#orgText').val();
            let output: any = '';

            if (source == '') {
                alert('원본 코드를 입력해주세요.');
                return;
            }

            const codeType = $('#codeType input:radio:checked').val();
            if (codeType == 'html') {
                if (looks_like_html(source)) {
                    output = window.html_beautify(source);
                    //console.log(output);
                } else {
                    output = window.js_beautify(source);
                    //console.log(output);
                }
            } else if (codeType == 'sql') {
                //output = sqlFormatter.format(source, {language: "sql"});
                alert('지원하지 않습니다!!');
            }

            $('#beautifyText').text(output);
        });

        // 문서종류 라디오 버튼
        $('#codeType :input').change(function () {
            $('#orgText').val('');
            $('#beautifyText').text('');
        });

        function looks_like_html(source: any): boolean {
            var trimmed = source.replace(/^[ \t\n\r]+/, '');
            return trimmed && trimmed.substring(0, 1) === '<';
        }

        return () => {
            $('#convertBtn').off('click');
        };
    });

    return (
        <>
            <SortSourceWrapper>
                <div className="container">
                    <div id="title">
                        <h1>
                            <b>코드 정렬</b>
                        </h1>
                        문서 종류를 선택 후 코드 입력 후 변환 버튼을 누르면 깔끔하게 정렬된 코드를 출력합니다.
                        <br />
                    </div>
                    <form className="form-horizontal">
                        <div className="form-group">
                            <div className="col-sm-offset-1 col-sm-10">
                                <div className="btn-group" id="codeType" data-toggle="buttons">
                                    <label className="btn btn-primary active">
                                        <input type="radio" name="codeType" autoComplete="off" value="html" />{' '}
                                        HTML/CSS/JS
                                    </label>
                                    <label className="btn btn-primary">
                                        <input type="radio" name="codeType" autoComplete="off" value="sql" /> SQL
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div className="form-group">
                            <label htmlFor="orgText" className="col-sm-1 control-label">
                                원본
                            </label>
                            <div className="col-sm-10">
                                <textarea
                                    id="orgText"
                                    name="orgText"
                                    className="form-control vresize"
                                    rows={10}
                                ></textarea>
                            </div>
                        </div>
                        <div className="form-group">
                            <label htmlFor="beautifyText" className="col-sm-1 control-label">
                                정렬
                            </label>
                            <div className="col-sm-10">
                                <pre id="beautifyText" className="prettyprint"></pre>
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="col-sm-offset-1 col-sm-10">
                                <button
                                    type="button"
                                    id="convertBtn"
                                    data-loading-text="처리중..."
                                    className="btn btn-primary"
                                >
                                    변환
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </SortSourceWrapper>
        </>
    );
}

export default SortSource;
