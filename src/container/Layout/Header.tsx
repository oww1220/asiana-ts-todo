import React, { useEffect, MouseEvent } from 'react';
import Datepicker from 'components/Input/Datepicker';
import { Link } from 'react-router-dom';
import jquery from 'jquery';
import useReactRouter from 'use-react-router';
import useLayout from 'hooks/UseLayout';
import useWrite from 'hooks/UseWrite';
import ButtonHover from 'components/ButtonHover/ButtonHover';
const $: JQueryStatic = jquery;

function Header() {
    const { history, location, match } = useReactRouter();
    const { changeDate, getListDefault } = useLayout();
    const { getWriteReset } = useWrite();

    const mainClick = (e: MouseEvent) => {
        let tg = $('#container .task-board');
        $('#search').val('');
        tg.find('.chk').each(function () {
            $(this).html(
                $(this)
                    .html()
                    .replace(/<span class="highLight".*?>(.*?)/gi, '')
                    .replace(/<\/span>/gi, '')
            );
        });
        history.push('/');
        changeDate(new Date());
        getListDefault();
        e.preventDefault();
    };

    const taskClcik = (e: MouseEvent) => {
        history.push('/Write');
        //getWriteReset();
        e.preventDefault();
    };

    /*
    useEffect(() => {
        console.log(location.pathname);
    }, [location.pathname]);
    */

    return (
        <header id="header">
            <h1>
                <a href="#" onClick={mainClick}>
                    <span className="name-main">ASIANA</span>{' '}
                    <span className="name-sub">
                        <b>AIRLINES</b>
                    </span>{' '}
                </a>
            </h1>
            <span className="app-version">v.{process.env.REACT_APP_VERSION}</span>
            {location.pathname.indexOf('/Write') === -1 && <Datepicker />}
            <div className="langs-link">
                <h2>현황판:</h2>
                <nav>
                    <a href="#" onClick={taskClcik} className="btn-data-write">
                        task 등록
                    </a>
                </nav>
            </div>
            <div className="function_wrap">
                <h2>업무 유틸리티:</h2>
                <Link to="/IeCopyWrap" target="_blank">
                    IE COPY
                </Link>
                <Link to="/MapToAnchor" target="_blank">
                    Anchor 변환
                </Link>
                <Link to="/UnEscape" target="_blank">
                    UnEscape
                </Link>
                <Link to="/VwCalculator" target="_blank">
                    PX to VW 변환
                </Link>
                <Link to="/CkEditor" target="_blank">
                    CkEditor
                </Link>
                <Link to="/SortSource" target="_blank">
                    HTML/JS 코드정렬
                </Link>
                <a href="./ImageMapGenrator.html" target="_blank">
                    이미지맵
                </a>
                <ButtonHover>
                    <a href="http://www.lonniebest.com/FormatCSS/" target="_blank">
                        CSS 코드정렬
                    </a>
                </ButtonHover>
            </div>
        </header>
    );
}

export default Header;
