import React from 'react';
import useReactRouter from 'use-react-router';
import Header from 'container/Layout/Header';
import Routes from 'components/Routes/Routes';
import Loading from 'components/Loading/Loading';
import BtTop from 'components/BtTop/BtTop';
import useLayout from 'hooks/UseLayout';
import { ResetGlobalStyle } from 'lib/Reset';

function Layout() {
    const { history, location, match } = useReactRouter();
    const { loadState } = useLayout();
    return (
        <>
            {!(
                location.pathname === '/CkEditor' ||
                location.pathname === '/VwCalculator' ||
                location.pathname === '/SortSource'
            ) && <ResetGlobalStyle />}

            {location.pathname !== '/IeCopyWrap' &&
            location.pathname !== '/MapToAnchor' &&
            location.pathname !== '/VwCalculator' &&
            location.pathname !== '/CkEditor' &&
            location.pathname !== '/SortSource' &&
            location.pathname !== '/UnEscape' ? (
                <>
                    <Header />
                    <div id="container">
                        <Routes />
                    </div>
                </>
            ) : (
                <Routes />
            )}
            {loadState && <Loading />}
            <BtTop />
        </>
    );
}

export default Layout;
